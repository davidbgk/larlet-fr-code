# Code.larlet.fr

## Installation

```
$ git clone https://gitlab.com/davidbgk/larlet-fr-code.git
$ cd larlet-fr-code
$ make venv
$ source venv/bin/activate
$ make install dev
$ make serve
```
