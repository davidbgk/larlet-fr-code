# HTML

## :octicons-repo-template-24: Un gabarit minimaliste

:hatching_chick: 2022-08

Parce qu’il faut bien copier-coller une source pour démarrer :fontawesome-solid-face-smile-beam:.


```html title="template.html" linenums="1"
--8<-- "code/html/template.html"
```

1. Pour en faire un document valide HTML5.
2. Pour les lecteurs d’écran, le référencement, les extensions, etc.
3. Doit être dans les premiers 1024 bytes, plutôt à mettre avant le `#!html <title>`, [référence à ce sujet](https://www.w3.org/TR/2012/CR-html5-20121217/document-metadata.html#charset).
4. Pourquoi pas de balise `meta` avec une valeur `X-UA-Compatible` ? Voir [cette réponse sur StackOverflow](https://stackoverflow.com/a/6771584).
5. Beaucoup de monde dans la balise `meta` relative au `viewport`, et [nous sommes responsables de cela…](https://codepen.io/tigt/post/meta-viewport-for-2015)
6. Avec cette valeur, le site lié ne saura pas de quelle page provient le lien qui a été cliqué mais aura uniquement l’information du domaine. Cela me semble être un bon compromis pour préserver l’intimité des personnes.
7. Gif transparent le plus petit qui soit, évite une requête inutile vers le serveur (et une erreur dans la console).
8. Ne doit pas être contenu dans une `#!html <section>`


## :selfie: Capturer une image/vidéo de la webcam depuis le navigateur

:hatching_chick: 2022-09

Exploration de [Austin Gil](https://austingil.com/html-capture-attribute/).

C’est uniquement possible sur certains navigateurs mobiles mais c’est bon à savoir.

```html title="capture.html" linenums="1"
<input type="file" accept="image/*" capture="environment">
```


## :material-table: Styler des colonnes de tableaux

:hatching_chick: 2022-10

J’ai toujours pensé qu’il fallait le faire à la main cellule par cellule mais en fait avec `colgroup`/`col` c’est tout à fait possible et en jouant avec `:target` on peut même le rendre interactif :

```html title="table-columns.html" linenums="1"
--8<-- "code/html/table-columns.html"
```

Pour la partie CSS :

```css linenums="1"
col:target {
  background: #dedede;
}
```

Trouvé [chez Manuel Matuzovic](https://www.matuzo.at/blog/highlighting-columns/).


## :material-cup: Faire grandir automatiquement une textarea

:hatching_chick: 2022-10

On commence avec le HTML suivant qui confère un parent (`grow-wrap`) au textarea ciblé :

```html linenums="1"
<div class="grow-wrap">
  <textarea name="text" id="text" 
    onInput="this.parentNode.dataset.replicatedValue = this.value"
    ></textarea>
</div>
```

!!! info Note
    
    La partie `onInput` pourrait être faite en JavaScript hors du HTML pour une réutilisation/séparation plus propre.

    Un exemple possible :

    ```js title="textarea-grow.js" linenums="1"
    --8<-- "code/javascript/textarea-grow.js"
    ```

La suite se passe en CSS, une fois la valeur de la `#!html textarea` dans le parent, on se sert de celui-ci pour obtenir la taille désirée du `:after` en fonction de l’espace utilisé.


```css title="textarea-grow.css" linenums="1"
--8<-- "code/css/textarea-grow.css"
```

Il y a un article complet sur [CSS-Tricks](https://css-tricks.com/the-cleanest-trick-for-autogrowing-textareas/) qui liste aussi des alternatives comportant davantage de JavaScript comme [celle de Jim Nielsen](https://blog.jim-nielsen.com/2020/automatically-resize-a-textarea-on-user-input/).


## :material-emoticon-sick-outline: Prendre en compte les préférences utilisateur·ice pour lancer une animation

:hatching_chick: 2022-10

J’ai découvert qu’il était possible d’afficher une image statique plutôt qu’un gif animé en fonction de la propriété `prefers-reduced-motion` directement dans le HTML :

```html linenums="1"
<picture>
  <source srcset="moonwalk.png" media="(prefers-reduced-motion: reduce)" />
  <img src="moonwalk.gif" alt="Someone doing the moonwalk" />
</picture>
```

Voir aussi ce qu’il est possible de faire [en CSS](css.md#prendre-en-compte-les-preferences-utilisateurice-pour-lancer-une-animation) et [en JS](javascript.md#prendre-en-compte-les-preferences-utilisateurice-pour-lancer-une-animation).

!!! info Bonus

    On peut aussi se servir des media-queries dans le HTML pour adapter les images d’un thème clair/sombre :

    ```html linenums="1"
    <picture>
      <source srcset="dark_logo.jpg" media="(prefers-color-scheme: dark)" />
      <img src="light_logo.jpg" alt="Homepage" />
    </picture>
    ```


## :material-theme-light-dark: Adapter le favicon au thème clair/sombre

:hatching_chick: 2022-10

Il est possible d’utiliser les _media-queries_ pour adapter les couleurs du _favicon_ en fonction du thème clair/foncé choisi.

```html title="favicon.svg" linenums="1"
<svg width="100" height="100" xmlns="http://www.w3.org/2000/svg">
  <style>
    circle {
      fill: yellow;
      stroke: black;
      stroke-width: 3px;
    }
    @media (prefers-color-scheme: dark) {
      circle {
        fill: black;
        stroke: yellow;
      }
    }
  </style>
  <circle cx="50" cy="50" r="47" />
</svg>
```

Pour ensuite une inclusion classique :

```html linenums="1"
<link rel="icon" href="/favicon.ico"> <!-- (1)! -->
<link rel="icon" href="/favicon.svg" type="image/svg+xml" />
```

1. Ça n’est [pas trop mal supporté](https://caniuse.com/link-icon-svg) mais ça mérite tout de même une valeur par défaut.

Ils en parlent [ici](https://blog.tomayac.com/2019/09/21/prefers-color-scheme-in-svg-favicons-for-dark-mode-icons/) et [là](https://www.matuzo.at/blog/html-boilerplate/).


## :material-web: Les favicons essentiels

:hatching_chick: 2022-10

Je me servais du [RealFaviconGenerator](https://realfavicongenerator.net/) mais il semblerait que ce ne soit plus nécessaire d’après un article de [Andrey Sitnik](https://evilmartians.com/chronicles/how-to-favicon-in-2021-six-files-that-fit-most-needs) qui réduit cela à 4 lignes :

```html title="favicons.html" linenums="1"
--8<-- "code/html/favicons.html"
```

Auxquelles il faut ajouter le `manifest.webmanifest` suivant :

```json linenums="1"
{
  "icons": [
    { "src": "/icon-192.png", "type": "image/png", "sizes": "192x192" },
    { "src": "/icon-512.png", "type": "image/png", "sizes": "512x512" }
  ]
}
```

Moins de fichiers… moins de maintenance :raised_hands: !

## :fontawesome-solid-arrows-turn-to-dots: Faire une redirection en HTML

:hatching_chick: 2022-11

Sans avoir la main sur le serveur, il est possible de faire des redirections depuis une page HTML.
Par exemple dans des environnements comme Github Pages ou certains générateurs de sites statiques :

```html title="redirect.html" linenums="1"
--8<-- "code/html/redirect.html"
```

## :material-close-thick: Un bouton pour fermer une modale

:hatching_chick: 2022-12

Il y a d’autres façons de le faire, [toute une liste est disponible sur HTMLHell](https://www.htmhell.dev/20-close-buttons/).

```html title="close-button.html" linenums="1"
--8<-- "code/html/close-button.html"
```

J’ai choisi celle-ci car elle est explicite et qu’elle évite d’annoncer le signe de multiplication (✕).


## :simple-letsencrypt: Générer des hashs SubResource Integrity (SRI)

:hatching_chick: 2022-12

Il s’agit d’un moyen de s’assurer que le fichier distant (CDN par exemple) téléchargé dans la page est celui escompté.
Cela donne une garantie supplémentaire face aux proxys douteux, aux attaques intermédiaires ou aux CDN qui se sont fait hackés.
Ce n’est pas magique non plus et pour la personne qui a également accès au HTML envoyé/reçu il est possible de modifier aussi la valeur de l’attribut `integrity`…

Il [existe des sites pour ça](https://www.srihash.org/) mais il est possible de le [générer à la main](https://www.htmhell.dev/adventcalendar/2022/3/) dans le terminal grâce à `openssl` :

```shell linenums="1"
 openssl dgst -sha384 -binary FILENAME.js | openssl base64 -A 
```

Le résultat doit ensuite être préfixé par l’algorithme (`sha384-` dans cet exemple) :

```html linenums="1"
<script src="https://unpkg.com/react@18/umd/react.production.min.js" 
  integrity="sha384-tMH8h3BGESGckSAVGZ82T9n90ztNXxvdwvdM6UoR56cYcf+0iGXBliJ29D+wZ/x8"
  crossorigin="anonymous"></script>
```

!!! warning

    L’attribut `crossorigin="anonymous"` est nécessaire lorsqu’il s’agit d’un fichier sur un autre domaine sinon le navigateur ne fait pas la vérification !

## :material-subtitles: Sémantique des sous-titres

:hatching_chick: 2023-01

Il ne s’agit pas de sous-titrer des vidéos mais de gérer un sous-titre à un titre principal sur une page web.

J’ai souvent utilisé le titre `#!html <hx>` suivant en fonction de l’arborescence en cours mais Steve Faulkner [propose une autre sémantique](https://www.tpgi.com/subheadings-subtitles-alternative-titles-and-taglines-in-html/) que je trouve intéressante :

```html title="subheadings.html" linenums="1"
--8<-- "code/html/subheadings.html"
```

En utilisant un `#!html <hgroup>`, cela permet d’avoir des sous-titres supérieurs ou inférieurs sans altérer une arborescence de titres dans la page.
Les balises ARIA viennent décrire explicitement la sémantique des différentes parties et de leurs relations.


## :octicons-single-select-16: Afficher une option par défaut pour un select requis

:hatching_chick: 2023-04

Pour un élément de formulaire de type `#!html <select>` requis, on veut tout de même afficher un premier choix sans qu’il soit pour autant possible de le soumettre.

Avec la combinaison des attributs `disabled` et `selected` on a le premier choix qui est sélectionné par défaut mais qui ne peut pas être soumis et qui n’est pas sélectionnable à nouveau lorsqu’on a choisi l’une des options possibles.

```html title="select-required.html" linenums="1"
--8<-- "code/html/select-required.html"
```

[Vous pouvez jouer avec ici (nouvel onglet)](examples/select-required.html){ .md-button target="_blank" }


## :octicons-tab-external-16: Onglets accessibles

:hatching_chick: 2023-11

!!! info Note
    
    Il y a beaucoup de façons d’implémenter cela qui sont plus ou moins valides sémantiquement et en matière d’accessibilité. Si la vôtre fonctionne, tant mieux ! Si vous pensez qu’il y a un défaut à celle proposée ci-dessous, n’hésitez pas à [proposer des améliorations](https://gitlab.com/davidbgk/larlet-fr-code/) ou à engager la discussion.

Je suis parti de [cet article](https://erikpoehler.com/2022/12/15/picocss-content-tabs/) en ajoutant une couche de sémantique/accessibilité que l’on peut trouver notamment [par là](https://inclusive-components.design/tabbed-interfaces/). Voir aussi la [documentation du W3C](https://www.w3.org/WAI/ARIA/apg/patterns/tabs/) à ce sujet.

On commence par le HTML :

```html title="tabs.html" linenums="1"
--8<-- "code/html/tabs.html"
```

1. Les rôles `tablist`, `tab` et `tabpanel` sont un moyen de rendre sémantiquement correcte cette implémentation.
2. On a un `id` et `aria-controls` sur le `tab` qui répondent à `id` et `aria-labelledby` sur le `tabpanel`.
3. J’ai souvent besoin de mettre le `#!html <label>` dans un `#!html <button>` pour aider à l’intégration dans une CSS existante mais ce n’est pas obligatoire.

Sans CSS ni JS, on a quand même le contenu qui s’affiche par défaut.

Un parti pris de ma part est de laisser tranquille les `tabindex` car j’ai peur de faire plus de mal qu’autre chose et l’argument de passer de _tab_ en _tab_ en bougeant les flèches du clavier ne **me** semble pas être très naturel. Ça va aussi dépendre de s’il y a des éléments focusables au sein des _tabpanel_.

Au lieu du combo `#!html <button>` + `#!html <label>`, on pourrait mettre une simple liste avec des liens qui font des ancres vers les sections. D’expérience, ça demande de prendre davantage la main niveau styles, à vous de voir.

On ajoute ensuite une couche de CSS pour que le `#!html <label>` active l’`#!html <input>` de type `checkbox` :

```css title="tabs.css" linenums="1"
--8<-- "code/css/tabs.css"
```

1. La « magie » se fait ici, c’est le tabpanel suivant l’input checké qui est affiché.

À cette étape-là, on a déjà quelque chose de fonctionnel, on pourrait ajouter une couleur de fond pour distinguer l’onglet actif (`#!css button:is([aria-selected="true"], :hover, :active, :focus)` par exemple).

On ajoute du JavaScript pour mettre à jour l’attribut `aria-selected` sur les boutons :

```js title="tabs.js" linenums="1"
--8<-- "code/javascript/tabs.js"
```

1. On simule un clic lorsqu’on tabule jusqu’à un onglet avant d’appuyer sur `Entrée` ou `Espace`.
2. La seule chose qui est finalement faite en JS c’est de modifier un attribut.

Notez le soin qui est apporté à essayer de se servir des [attributs sémantiques](https://benmyers.dev/blog/semantic-selectors/) plutôt que d’ajouter des classes ou de dépendre de la structure HTML.

[Vous pouvez jouer avec ici (nouvel onglet)](examples/tabs.html){ .md-button target="_blank" }

Pour une [version utilisant un Web Component](https://www.mayank.co/blog/tabs).

## :material-view-list: Forcer un élément parmi une `datalist`

:hatching_chick: 2024-02

On peut ajouter des suggestions choisies sur un `#!html <input>` à l’aide de 
`#!html <datalist>` mais par défaut il est possible de soumettre un élément qui n’est 
pas proposé dans la liste initiale.

Pour forcer ce comportement, il faut ajouter un attribut `pattern` qui liste lui aussi
l’intégralité des options disponibles.

```html title="datalist-forced.html" linenums="1"
--8<-- "code/html/datalist-forced.html"
```

1. Notez qu’il est nécessaire d’échapper tout ce qui ressemble à une 
   expression régulière !

Si vous avez besoin de vérifier le comportement, vous pouvez ajouter 
ces lignes de CSS :

```css linenums="1"
input[list]:user-invalid {
  background-color: lightpink;
  border: 2px solid red;
}
```

