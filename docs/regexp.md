# RegExps

## :octicons-link-16: Parser une URL

:hatching_chick: 2022-08

Une expression régulière qui permet de vérifier qu’une chaîne de
caractère est bien une URL et de récupérer ses différentes parties.

Elle a originellement été postée [par John Gruber](https://gist.github.com/gruber/8891611) mais ensuite
enrichie [par Tom Winzig](https://gist.github.com/winzig/8894715) pour une version qui supporte notamment
les domaines internationaux.

``` title="url.regexp" linenums="1"
--8<-- "code/regexp/url.regexp"
```

Cette expression régulière a été élevée [dans le domaine public](https://gist.github.com/winzig/8894715?permalink_comment_id=4023641#gistcomment-4023641).

!!! bug "TODO"
    Ça serait pas mal d’avoir une version Python avec des groupes nommés `(?P<tld>...)`


## :octicons-image-16: Parser une image commonmarkdown

:hatching_chick: 2022-08

J’apprécie tellement que les expressions régulières Python soient
documentables et permettent d’accéder aux groupes nommés aussi 
facilement !

```py title="regexp-image-md.py" linenums="1"
--8<-- "code/python/regexp-image-md.py"
```

Et voici un exemple d’usage :

```py title="regexp-image-md-example.py" linenums="1" hl_lines="2"
images_dict = []
images_properties = MD_IMAGE_PATTERN.findall(md_content)  # noqa (1)
if images_properties:
    for image_properties in images_properties:
        images_dict.append(dict(zip(("alt", "url", "title"), image_properties)))
```

1.  On annule la vérification `flake8` car ces variables n’existent pas
    dans le contexte actuel.


## :material-walk: Pour aller plus loin/différemment

:hatching_chick: 2022-09

* [Regular expression 101](https://regex101.com/)
* [RegExr](https://regexr.com/)


