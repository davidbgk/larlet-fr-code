# CSS

## :fontawesome-solid-scroll: Scroller en douceur

:hatching_chick: 2022-08

```css title="smooth.css" linenums="1"
--8<-- "code/css/smooth.css"
```


## :material-emoticon-sick-outline: Prendre en compte les préférences utilisateur·ice pour lancer une animation

:hatching_chick: 2022-08

```css title="motion.css" linenums="1"
--8<-- "code/css/motion.css"
```

Une autre option est de le faire directement en ne chargeant la CSS
dédiée aux animations que si la préférence utilisateur·ice
la rend acceptable :

```css title="motion-link.html" linenums="1"
--8<-- "code/html/motion-link.html"
```

Voir aussi ce qu’il est possible de faire [en HTML](html.md#prendre-en-compte-les-preferences-utilisateurice-pour-lancer-une-animation) et [en JS](javascript.md#prendre-en-compte-les-preferences-utilisateurice-pour-lancer-une-animation).



## :octicons-video-16: Avoir un bon ratio pour les vidéos

:hatching_chick: 2022-08

Une façon [documentée par ici](https://timothymiller.dev/posts/2022/stop-using-the-padding-hack/) pour afficher des incrustations
vidéos dans un format convenable :

```css title="video-ratio.css" linenums="1"
--8<-- "code/css/video-ratio.css"
```


## :material-radiobox-blank: Un reset minimaliste

:hatching_chick: 2022-08

Un reset très bien [documentée par ici](https://www.joshwcomeau.com/css/custom-css-reset/) qui est relativement
court :

```css title="reset.css" linenums="1"
--8<-- "code/css/reset.css"
```

J’ajoute souvent aussi ces quelques règles que j’estime être importantes
pour avoir des styles par défaut cohérents :

```css title="reset-extras.css" linenums="1"
--8<-- "code/css/reset-extras.css"
```

Il y a d’autres projets comme [minireset.css](https://github.com/jgthms/minireset.css) qui existent.


## :material-domino-mask: Masquer du contenu

:hatching_chick: 2022-08

Il y a plusieurs façons de le faire mais je trouve que la meilleure
explication vient de Kitty Giraudel à travers [deux](https://kittygiraudel.com/2020/12/03/a11y-advent-hiding-content/) [articles](https://kittygiraudel.com/2020/12/06/a11y-advent-skip-to-content/)
de 2020.

```css title="screen-readers-only.css" linenums="1"
--8<-- "code/css/screen-readers-only.css"
```

C’est ensuite utilisable ainsi, par exemple pour un lien d’accès direct
au contenu où l’on a besoin de conserver le focus :

```html
<a href="#main" class="sr-only sr-only--focusable">Skip to content</a>
```


## :material-oil: Changement de graisse

:hatching_chick: 2022-08

Lorsqu’on clic sur un élément pour le rendre **gras** (ou l’inverse),
ça modifie l’espace réservé par le navigateur pour ce(s) terme(s).
Parfois, cela peut engendrer des effets disgracieux (hover sur un lien,
item actif d’un onglet, etc).

Une solution [trouvée par ici](https://darn.es/building-tabs-in-web-components/#adjusting-for-tab-weight-changes) est de charger le contenu en **gras**
dans un `#!css ::before`

```css title="reserve-bold-space.css" linenums="1"
--8<-- "code/css/reserve-bold-space.css"
```

On peut ensuite l’appeler ainsi dans le HTML :

```html linenums="1"
<div class="reserve-bold-space" data-text="The tab label">
    The tab label
</div>
```

Notez bien qu’il faut que le contenu du `#! data-text` soit le même que
le contenu affiché !


## :fontawesome-solid-medal: Augmenter la spécificité d’une classe

:hatching_chick: 2022-08

Si vous n’avez accès qu’à une classe et qu’il vous faut la spécificité
d’un id par exemple, il est possible d’utiliser `#!css :not()` avec un
id ce qui va artificiellement passer ce sélecteur à une spécificité d’id
même si celui-ci n’existe pas ! Merci [Robert Koritnik](https://erraticdev.blogspot.com/2018/11/css-class-selector-with-id-specificity.html).

```css title="bump-specificity.css" linenums="1"
--8<-- "code/css/bump-specificity.css"
```

Pour tester les spécificités en CSS : [Specificity Calculator](https://specificity.keegan.st/)


## :scientist: Cas d’usages de `#!css is()` et `#!css has()`

:hatching_chick: 2022-08

!!! danger "Support navigateur"
    Attention, si `#!css is()` n’est [pas trop mal supporté](https://caniuse.com/css-matches-pseudo),
    `#!css has()` n’est [pas implémenté par de nombreux navigateurs](https://caniuse.com/css-has)
    à ce jour (22 août 2022). Je consigne les cas d’usage ici pour
    l’avenir mais ça n’est pas envisageable en production pour l’instant.

    Il est possible de tester son existence avec `#!css @supports` + `#!css selector()` :

    ```css
    @supports (selector(:has(works))) {
      /* safe to use :has() */
    }
    ```

Plein d’exemples qui viennent [du blog de webkit](https://webkit.org/blog/13096/css-has-pseudo-class/).

Lorsque tu veux que les éléments qui suivent un titre soient proches
de ce titre :

```css title="has-titles.css" linenums="1"
--8<-- "code/css/has-titles.css"
```

Lorsque tu veux que ton formulaire t’indique un peu mieux où est
l’erreur :

```css title="has-errors.css" linenums="1"
--8<-- "code/css/has-errors.css"
```

Ici, j’applique un style au `#!css label` mais ça pourrait être
plus large bien entendu, ça ouvre de très nombreuses possibilités !

Lorsque tu veux changer des variables CSS en fonction d’un
choix utilisateur·ice (par exemple un sélecteur de thème) :

```css title="has-choice.css" linenums="1"
--8<-- "code/css/has-choice.css"
```

Ça ne fait qu’effleurer les possibilités mais ça promet :material-robot-happy-outline:.

Un [article par Bramus Van Damme](https://www.bram.us/2021/12/21/the-css-has-selector-is-way-more-than-a-parent-selector/) à ce sujet ainsi qu’un
[article sur le blog de Chrome](https://developer.chrome.com/blog/has-m105/) qui donnent d’autres exemples.


## :material-tools: Trouver les déclarations CSS non utilisées

:hatching_chick: 2022-10

Un petit script à copier-coller dans sa console pour afficher toutes les déclarations CSS qui ne sont pas utilisées dans la page en cours (inspiré [de cet article](http://codemadness.org/query-unused-css-rules-on-current-document-state.html), ce site est une mine).

```javascript title="unused-css.js" linenums="1"
--8<-- "code/javascript/unused-css.js"
```

!!! info Note

    Les CSS sont généralement utilisées pour plusieurs pages à la fois donc ça peut ne pas être très pertinent mais ça donne quand même une aperçu de ce qui pourrait être optimisé pour l’affichage de cette seule page !


## :fontawesome-solid-scroll: Scroll et sticky header

:hatching_chick: 2022-11

La propriété `scroll-margin-top` permet de gérer la distance depuis le haut de la page et l’endroit jusqu’où scrolle la page lorsqu’il y a une ancre ciblée.
C’est particulièrement intéressant dans un contexte de _header_ `sticky` :

```css title="scroll-sticky-header.css" linenums="1"
--8<-- "code/css/scroll-sticky-header.css"
```

1. La taille du texte plus les deux _paddings_.


## :material-file-tree: Une arborescence à base de `ul` et de `details`

:hatching_chick: 2022-11

Un [article détaillé de Kate Rose Morley](https://iamkate.com/code/tree-views/) pour recréer une arborescence en CSS en combinant des `ul`/`li` et des `details`/`summary`, particulièrement malin !

```css title="tree-ul-details.css" linenums="1"
--8<-- "code/css/tree-ul-details.css"
```

Par ici un exemple complet assez brut car c’est toujours chouette de jouer avec en direct :

[Ouvrir dans un nouvel onglet](examples/css-tree.html){ .md-button target="_blank" }


## :material-invert-colors: Utiliser `hsl()` et des variables CSS pour décliner des styles

:hatching_chick: 2023-01

Manuel Matuzović [montre comment](https://www.matuzo.at/blog/2023/hsl-custom-properties/) utiliser des variables CSS et la fonction `#!css hsl()` pour adapter un design en ne changeant qu’une seule variable de teinte (_hue_) de couleur :

```css title="hsl-css-variables.css" linenums="1"
--8<-- "code/css/hsl-css-variables.css"
```

C’est fascinant car cela permet de jouer avec la variable dans l’inspecteur de styles, par exemple en modifiant le curseur j’arrive rapidement à :

```css linenums="1"
.valid {
  --h: 120deg;
}
```


## :fontawesome-solid-highlighter: Mettre en surbrillance la destination d’une ancre

:hatching_chick: 2023-11

Vient [de là](https://phuoc.ng/collection/css-animation/highlighting-target-element/) et peut être appliqué à une nouvelle entrée sur la page aussi par exemple :

```css title="highlight.css" linenums="1"
--8<-- "code/css/highlight.css"
```

1. Pour que ça s’applique à une ancre, à adapter.


## :material-arrow-expand: Une grille avec éléments à fond perdu

:hatching_chick: 2023-12

!!! info
    Le « fond perdu » (ou « débord » ou « bord perdu ») est une expression consacrée pour une impression sans marges. Vous aussi vous venez de l’apprendre ?

Merci à [Josh W. Comeau](https://www.joshwcomeau.com/css/full-bleed/) pour cette astuce :

```css title="grid-full-bleed.css" linenums="1"
--8<-- "code/css/grid-full-bleed.css"
```

1. On définit une colonne centrale de `60ch` pour la visibilité et on prend en compte le `gap * 2` pour la version petits écrans.
2. On met tout le contenu dans la colonne du milieu (seconde ici).
3. On se donne la possibilité avec une classe de pouvoir étendre un élément à la largeur totale de la fenêtre.

:hatched_chick: 2024-06

Andrew Walpole a créé un outil permettant de définir plusieurs niveaux, [Layout Breakouts Builder](https://layout-breakouts-builder.vercel.app/), dont l’exemple par défaut (4 niveaux) est :

```css title="layout-breakouts-builder.css" linenums="1"
--8<-- "code/css/layout-breakouts-builder.css"
```


## :material-rounded-corner: Des éléments arrondis imbriqués et homogènes

:hatching_chick: 2024-06

Un article de Andy Bell, [Relative rounded corners](https://set.studio/relative-rounded-corners/), qui explique comment faire en sorte que des bords arrondis imbriqués conservent un radius homogène plus plaisant à l’œil :

```css title="matched-radius.css" linenums="1"
--8<-- "code/css/matched-radius.css"
```

La beauté des variables CSS, c’est qu’il est possible de les écraser directement depuis le HTML au besoin. Par exemple :

```html
<div class="matched-radius"
     style="--matched-radius-inner-size: 15px;--matched-radius-padding: 4px;">
    <div class="matched-radius__inner">
    </div>
</div>
```


## :material-page-layout-header: Un header masqué au scroll qui réapparait

:hatching_chick: 2024-06

Un article de Andrew Walpole, [The Showy / Hidey Navigation Bar](https://andrewwalpole.com/blog/the-showy-hidey-nav-bar/) qui documente un pattern qui je n’affectionne pas vraiment mais ça peut être utile dans certaines circonstances :


```css title="showy-hidey-header.css" linenums="1"
--8<-- "code/css/showy-hidey-header.css"
```

1. « animer/transitionner `top`, c’est mal ! » dit [Rik](https://ricaud.me/blog/).

Il est à noter que cela nécessite d’avoir également un bout de JS pour gérer le calcul du masquage (scroll down) / réaffichage (scroll up) :


```js title="showy-hidey-header.js" linenums="1"
--8<-- "code/javascript/showy-hidey-header.js"
```


## :material-button-cursor: Pour des boutons plus résilients

:hatching_chick: 2024-10

Un article de David Bushell [sur des propriétés CSS moins connues](https://dbushell.com/2024/03/10/css-button-styles-you-might-not-know/) relatives aux boutons :


```css title="button-styling.css" linenums="1"
--8<-- "code/css/button-styling.css"
```


## :material-cursor-pointer: Un curseur dédié pour pointer des éléments

:hatching_chick: 2024-10

Je suis pas mal de [l’avis de Chris Ferdinandi](https://gomakethings.com/adding-cursors-to-stuff-for-better-ux/) concernant l’importance de changer le type de curseur au survol d’éléments comme `#!html <summary>` :


```css title="cursor-pointer.css" linenums="1"
--8<-- "code/css/cursor-pointer.css"
```

!!! note

    Si vous avez une référence solide qui vient expliquer pourquoi cela n’est pas une bonne idéé, notamment en matière d’accessibilité, n’hésitez pas à me contacter !


## :material-language-css3: Exemples de CSS minimalistes / sans classe

:hatching_chick: 2022-10

* [Simple.css](https://simplecss.org/)
* [Basic.css](https://vladocar.github.io/Basic.css/)
* [new.css](https://newcss.net/)
* [Water.css](https://watercss.kognise.dev/)
* [Almond.css](https://alvaromontoro.github.io/almond.css/)
* [Neat CSS](https://github.com/codazoda/neatcss)
* [Bolt CSS](https://boltcss.com/)
* [Marx](https://mblode.github.io/marx/) (forcément)
* [Toute une liste ici](https://github.com/dbohdan/classless-css)


## :fontawesome-solid-seedling: Pour progresser

:hatching_chick: 2024-10

* [SmollCSS](https://smolcss.dev/)
* [TODS](https://github.com/clagnut/TODS)
* [CubeCSS](https://cube.fyi/)
* [kraken](https://cferdinandi.github.io/kraken/)
* [picoCSS](https://picocss.com/)

## :material-nintendo-game-boy: Pour jouer

:hatching_chick: 2022-10

* [CSS Grid Garden](https://cssgridgarden.com/#fr)
* [Flexbox Froggy](https://flexboxfroggy.com/#fr)
* [CSS Battle](https://cssbattle.dev)
