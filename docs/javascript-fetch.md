# Fetch et JavaScript

!!! info "Une page dédiée ?"
    Malheureusement, oui, `#!js fetch()` a besoin d’une page dédiée
    car cette fonctionnalité — bien que récente — n’est 
    vraiment pas bien finie…

## :currency_exchange: Sérialisation des données

:hatching_chick: 2022-08

Un [article de Baldur Bjarnason](https://www.baldurbjarnason.com/2021/fetch-and-formdata/) explique comment utiliser
`#!js FormData` pour sérialiser les données qui proviennent d’un
formulaire, soit pour du classique `urlencoded` soit pour du JSON.

```js title="fetch-serialize-form.js" linenums="1" hl_lines="5 7"
--8<-- "code/javascript/fetch-serialize-form.js"
```


```js title="fetch-serialize-json.js" linenums="1" hl_lines="5 7"
--8<-- "code/javascript/fetch-serialize-json.js"
```


## :material-note-off: Vérifier la nature de la réponse d’erreur d’une API

:hatching_chick: 2022-08

Une [astuce de Chris Ferdinandi](https://gomakethings.com/how-to-check-if-an-api-error-response-is-json-or-not-with-vanilla-javascript/) pour évaluer la bonne erreur à
afficher lorsqu’elle provient d’une API, on utilise le header de
`content-type` pour déterminer la méthode à appeler sur l’erreur.

```js title="fetch-api-error.js" linenums="1" hl_lines="13-18"
--8<-- "code/javascript/fetch-api-error.js"
```


## :material-bridge: Choisir/deviner les formats en entrée/sortie

:hatching_chick: 2022-08

Un autre extrait d’un ancien projet où on voulait pouvoir envoyer
un hash ou un FormData et récupérer du JSON ou du texte.

```js title="fetch-handle-formats.js" linenums="1"
--8<-- "code/javascript/fetch-handle-formats.js"
```

En entrée, on redéfinit le `body` de notre requête en fonction de sa
nature (détectée automagiquement). En sortie, on regarde le content-type
pour appliquer le handler approprié.


## :material-link-off: Retourner l’URL qui a causé l’erreur

:hatching_chick: 2022-08

On voudrait par exemple afficher le domaine qui a causé l’erreur,
étonnamment ça n’est pas trivial :


```js title="fetch-url-error.js" linenums="1" hl_lines="10 12 20 27"
--8<-- "code/javascript/fetch-url-error.js"
```

Que l’on va ensuite pouvoir utiliser ainsi :

```js linenums="1"
request(url)
  .then(response => {
    // Do something.
  })
  .catch(handleError)
```

Ce qui permettra d’afficher une erreur personnalisée contenant le 
domaine concerné sans avoir à passer l’URL dans la méthode qui gère 
les erreurs.


## :material-walk: Aller plus loin/différemment

* [A library that makes the Fetch API a breeze ](https://github.com/zellwk/zl-fetch)
* [Convenient parsing for Fetch.](https://github.com/d3/d3-fetch)
* [Using fetch](https://css-tricks.com/using-fetch/)

