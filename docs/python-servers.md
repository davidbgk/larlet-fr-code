# Serveurs en Python

## :material-rocket-launch: Celui par défaut

:hatching_chick: 2022-08

La base :material-emoticon-wink:

<div class="termy">

```console
$ python -m http.server 8000
Serving HTTP on :: port 8000 (http://[::]:8000/) ...
```

</div>

Pour sortir/arrêter : ++ctrl+c++

??? note "Et avec Python 2 ?"

    Bon déjà pas de bol… mais ça reste possible :  
    `#!console $ python -m SimpleHTTPServer 8000`


## :hatching_chick: Le plus petit

:hatching_chick: 2022-08

```py title="server.py" linenums="1"
--8<-- "code/python/server.py"
```

Dans un shell :

<div class="termy">

```console
$ python server.py
```

</div>

Et dans l’autre :

<div class="termy">

```console
$ curl -D - localhost:8001
HTTP/1.0 200 OK
Server: SimpleHTTP/0.6 Python/3.9.13
Date: Sat, 13 Aug 2022 14:50:37 GMT

Hello world
```

</div>

Vous devriez voir passer la requête dans votre précédent shell :

<div class="termy">

```console
$ python server.py
127.0.0.1 - - [13/Aug/2022 10:50:37] "GET / HTTP/1.1" 200 -
```

</div>

## :baby_chick: Avec moins de dépendances

:hatching_chick: 2022-08

Une autre approche avec un [article très didactique](https://www.codementor.io/@joaojonesventura/building-a-basic-http-server-from-scratch-in-python-1cedkg0842) sur le sujet
qui utilise encore moins de dépendances (mais ça demande d’en faire 
un peu plus aussi…) :

```py title="server-socket-only.py" linenums="1"
--8<-- "code/python/server-socket-only.py"
```

Il est un peu plus complet aussi car il s’occupe un peu des routes
pour afficher des fichiers HTML.


## :chicken: Plus complet

:hatching_chick: 2022-08

Le [plus petit framework](https://github.com/kadircancetin/MostMinimalWebFramework) en Python (100 lignes !).

```py title="framework.py" linenums="1" hl_lines="0-100"
--8<-- "code/python/framework.py"
```

Avec des exemples d’usages ensuite dans le fichier pour les routes, etc.

## :material-walk: Pour aller plus loin/différemment

* [Roll](https://roll.readthedocs.io/en/latest/)
* [Quart](https://github.com/pallets/quart)
* [Nameko](https://github.com/nameko/nameko)
