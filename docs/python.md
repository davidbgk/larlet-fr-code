# Python

## :fontawesome-solid-people-line: Voisinage

:hatching_chick: 2022-08

Lorsqu’on parcoure une liste, il est parfois utile d’avoir accès à
l’élément précédent et au suivant, pour faire par exemple des liens
suivant/précédent entre des billets de blog.

Cette implémentation permet de spécifier le premier et le dernier item
de la liste au besoin, ce qui permet de lier vers d’autres contenus
par exemple.

```py title="neighborhood.py" linenums="1" hl_lines="9 10"
--8<-- "code/python/neighborhood.py"
```

1.  :man_raising_hand: `iterable` peut être une liste, un set ou toute structure… itérable !

Tout se passe au moment du `#!python yield`, on garde en mémoire le 
précédent et le courant lors du parcours de l’itérateur et… on n’oublie
pas le dernier !


## :fontawesome-solid-list: Lister des dossiers/fichiers

:hatching_chick: 2022-08

Il est courant en Python de devoir parcourir des fichiers ou des dossiers.
Ces deux méthodes permettent de récupérer les chemins des dossiers ou
des fichiers à partir d’une racine.

```py title="each-from.py" linenums="1"
--8<-- "code/python/each-from.py"
```

1.  Le paramètre de `pattern` est bien pratique pour ne récupérer 
    que des `*.md` par exemple.


## :fontawesome-solid-hashtag: Générer un md5

:hatching_chick: 2022-08

Parce que j’oublie à chaque fois comment faire…

```py title="md5.py" linenums="1"
--8<-- "code/python/md5.py"
```

Il y aurait probablement de meilleurs algorithmes à utiliser selon les cas.


## :material-link: Générer une chaîne de 128 bits

:hatching_chick: 2022-08

Pour avoir des slugs dans une URL par exemple.

```py title="128bits-string.py" linenums="1"
--8<-- "code/python/128bits-string.py"
```

Pour un usage réel, il faudrait exclure certains caractères qui peuvent
prêter à confusion. Mais qui dicte des URLs ?

:hatching_chick: 2022-12

Une autre façon de générer des tokens aléatoires :

```sh
$ python3 -c 'print(__import__("secrets").token_hex(16))'
fcaef3ee71f32009fe240661fffa693c
```

## :material-data-matrix: Générer une data URI

:hatching_chick: 2022-08

Il est possible de transformer des images en chaîne de caractère afin
de les insérer directement dans le HTML.

```py title="data-uri.py" linenums="1"
--8<-- "code/python/data-uri.py"
```


## :material-database-plus: Utiliser des ULID (vs. UUID)

:hatching_chick: 2022-09

Pour obtenir de l’unicite (pour des clés primaires par exemple), mieux que des UUID, il y a les ULID :

* [Spécification](https://github.com/ulid/spec)
* [Bibliothèque en Python](https://github.com/ahawker/ulid)
* [Une autre bibliothèque en Python](https://github.com/mdomke/python-ulid)
* [Un article sur les différences/avantages](https://sudhir.io/uuids-ulids)


## :material-update: Récupérer la valeur epoch depuis une date ISO

Parce que j’ai été surpris que ce soit aussi compliqué.
Je suis peut-être passé à côté d’un truc plus élégant…

```py title="to-epoch.py" linenums="1"
--8<-- "code/python/to-epoch.py"
```

1.  Désactivation de black :
    on veut faire rentrer le code dans cette page manuellement.

!!! bug "TODO"
    Configurer `black` pour avoir des lignes plus courtes par défaut.

## :mobile_phone: Mot de passe à usage unique

:hatching_chick: 2022-10

Un bout de code qui [vient de SourceHut](https://drewdevault.com/2022/10/18/TOTP-is-easy.html) pour générer un [mot de passe à usage unique basé sur le temps](https://fr.wikipedia.org/wiki/Mot_de_passe_%C3%A0_usage_unique_bas%C3%A9_sur_le_temps).


```py title="totp-2fa.py" linenums="1"
--8<-- "code/python/totp-2fa.py"
```

Je ne comprends pas tout mais j’espère pouvoir m’en resservir un jour (et le comprendre à ce moment là…).

La version anglaise que je ne me risque pas à frangliser :

> The algorithm is as follows:
>
> 1. Divide the current Unix timestamp by 30
> 2. Encode it as a 64-bit big endian integer
> 3. Write the encoded bytes to a SHA-1 HMAC initialized with the TOTP shared key
> 4. Let offs = hmac[-1] & 0xF
> 5. Let hash = decode hmac[offs .. offs + 4] as a 32-bit big-endian integer
> 6. Let code = (hash & 0x7FFFFFFF) % 1000000
> 7. Compare this code with the user’s code
>
> You’ll need a little dependency to generate QR codes with the [otpauth:// URL scheme](https://github.com/google/google-authenticator/wiki/Key-Uri-Format), a little UI to present the QR code and store the shared secret in your database, and a quick update to your login flow, and then you’re good to go.
> 
> This implementation has a bit of a tolerance added to make clock skew less of an issue, but that also means that the codes are longer-lived.


## :material-git: Installer un paquet depuis une branche distante avec pip

:hatching_chick: 2023-01

Il est possible d’installer un paquet depuis une branche distante avec l’utilitaire `pip` avec cette ligne de commande :

```shell
pip install https://github.com/<user>/<repo>/archive/refs/heads/<branch>.zip
```

C’est très pratique pour donner du _feedback_ sur une _pull-request_ par exemple :

```shell
pip install https://github.com/simonw/datasette-insert/archive/refs/heads/cors.zip --upgrade
```

Ou pour installer la dernière version de la branche principale :

```shell
pip install https://github.com/buriy/python-readability/archive/refs/heads/master.zip --upgrade
```


## :material-butterfly: Faire des requêtes HTTP en Python sans lib

:hatching_chick: 2023-01

J’ai découvert cela grâce au [AdvancedRestClient](https://github.com/advanced-rest-client/arc-electron) qui propose des exemples de code en Python pour reproduire les requêtes réalisées via l’outil, dont une version avec les batteries inclues :

```py title="native-http-request.py" linenums="1"
--8<-- "code/python/native-http-request.py"
```

Je ne l’ai pas encore utilisé car je favorisais jusqu’à présent [HTTPX](https://www.python-httpx.org/) qui est maintenu par des personnes en qui j’ai confiance.


## :material-timer-sand: Tâches asynchrones

:hatching_chick: 2022-09

Surtout des liens vers les projets existants dont j’ai toujours du mal à me rappeler :

* [RQ (Redis Queues)](https://python-rq.org/) : is a simple Python library for queueing jobs and processing them in the background with workers.
* [Procrastinate](https://procrastinate.readthedocs.io/en/stable/) : PostgreSQL-based Task Queue for Python
* [wakaq](https://github.com/wakatime/wakaq) : Distributed background task queue for Python backed by Redis, a super minimal Celery
* [Dramatiq](https://dramatiq.io/) : Dramatiq is a background task processing library for Python with a focus on simplicity, reliability and performance. 
* [huey](https://huey.readthedocs.io/en/latest/) : a lightweight alternative.
* [Nameko](https://nameko.readthedocs.io/en/stable/) : A microservices framework for Python that lets service developers concentrate on application logic and encourages testability.
* [Background Tasks](https://www.starlette.io/background/) : Starlette includes a BackgroundTask class for in-process background tasks. A background task should be attached to a response, and will run only once the response has been sent.
* [Chard](https://github.com/drpancake/chard) : is a very simple async/await background task queue for Django. One process, no threads, no other dependencies.

## :card_box: Forcer un environnement virtuel

:hatching_chick: 2023-10

J’avais l’habitude de mettre dans mes Makefile :

```shell
python3 -m pip install --editable .
```

Si on veut s’assurer que ce ne sera pas fait en dehors d’un environnement virtuel, 
il est possible d’ajouter :

```shell
python3 -m pip --require-virtualenv install --editable .
```

Il est aussi possible de configurer `pip` pour le faire de manière globale sur son système :

```shell
python3 -m pip config set global.require-virtualenv True
```

Sous macOS, cela va créer un fichier `~/.config/pip/pip.conf` avec le contenu suivant :

```ini
[global]
require-virtualenv = True
```

## :material-web-remove: Retirer les accents d’une chaîne de caractères

:hatching_chick: 2023-10

On utilise la lib standard avec `unicodedata` qui permet de [découper les caractères](https://docs.python.org/3/library/unicodedata.html#unicodedata.normalize) sous leur forme normale.

On exclue les caractères qui sont dans la catégorie des [Nonspacing_Mark](http://www.unicode.org/reports/tr44/#GC_Values_Table) (`Mn`), correspondant à des marqueurs de caractères diacritiques (soyons honnête, je n’ai pas tout compris mais ça semble marcher…).

```py title="strip-accents.py" linenums="1"
--8<-- "code/python/strip-accents.py"
```

1.  Désactivation de black :
    on veut faire rentrer le code dans cette page manuellement.


## :material-tray-remove: Retirer la ponctuation d’une chaîne de caractères

:hatching_chick: 2023-10

J’aime bien cette solution car elle utilise au maximum la bibliothèque standard 🐍🔋.

```py title="strip-punctuation.py" linenums="1"
--8<-- "code/python/strip-punctuation.py"
```

1.  Ces caractères additionnels sont intéressants pour des textes en français, 
    vous pouvez les adapter.
2.  Ici on remplace chaque caractère de ponctuation par une espace simple, 
    à adapter aussi !


## :material-set-split: Extraire les mots signifiants d’un texte

:hatching_chick: 2023-10

```py title="filter-words.py" linenums="1"
--8<-- "code/python/filter-words.py"
```

1.  C’est la première fois que je me permettais d’utiliser le _walrus operator_ sur un projet.
    Pas sûr que ce soit vraiment plus lisible hein 😅

L’idée est de combiner les trois précédents bouts de code ainsi :

```py title="example.py" linenums="1"
data = f"{description} {mots_cles} {solutions}"
data = strip_punctuation(data)
data = " ".join(filter_words(data))
data = strip_accents(data)
```


Voir aussi [cette entrée JavaScript](javascript.md#rechercher-des-mots-dans-un-texte) pour avoir la logique côté client.


## :material-file-compare: Comparer des chaînes de caractères

:hatching_chick: 2023-12

James Bennett [explique en détail](https://www.b-list.org/weblog/2023/dec/23/compare-python-strings/) pourquoi et je découvre au passage `#!py str.casefold()` qui est [bien plus pertinent](https://docs.python.org/3/library/stdtypes.html#str.casefold) qu’un `#!py str.lower()` pour des comparaisons :

```py title="compare-strings.py" linenums="1"
--8<-- "code/python/compare-strings.py"
```


## :fontawesome-solid-file-csv: Télécharger une feuille Google en CSV

:hatching_chick: 2023-10

C’est une chose qui m’arrive très souvent car j’apprécie de travailler avec des personnes qui ne sont pas techniques et je ne veux pas (immédiatement :p) leur imposer _mes_ outils de saisie de données.

```py title="download-googlesheet-as-csv.py" linenums="1"
--8<-- "code/python/download-googlesheet-as-csv.py"
```

1.  À passer en paramètre de la fonction au besoin.

Ce n’est pas destiné à être trop robuste mais de toute façon les URLs de Google sont encore moins pérennes…


## :fontawesome-solid-memory: Générer un ZIP de fichiers tout en mémoire

:hatching_chick: 2023-11

!!!info
    Je mets la vue Django complète extraite du code de uMap car ça donne un cas concrêt.

Ici on veut boucler sur les geojson des cartes et en faire un zip à télécharger.
Mais sans pour autant stocker les fichiers geojson _ni_ le zip final donc tout passe par des objets `BytesIO` et `StringIO`.

```py title="zip-in-memory.py" linenums="1"
--8<-- "code/python/zip-in-memory.py"
```

J’étais bien content de pouvoir faire ça en quelques lignes car j’avais un doute sur la faisabilité.

_Si vous avez besoin de le faire [côté client en JavaScript](javascript.md#generer-un-fichier-zip-a-telecharger)._


## :material-script-text: Script Python autonome avec uv

:hatching_chick: 2024-10

Un moyen de rendre n’importe quel script Python exécutable sous réserve qu’il y ait [uv](https://docs.astral.sh/uv/) d’installé sur la machine (ce qui est moins contraignant/fastidieux que d’expliquer ce qu’est un _virtualenv_…). Merci [Simon Willison](https://simonwillison.net/2024/Aug/21/usrbinenv-uv-run/) !

```py title="uv-script.py" linenums="1"
--8<-- "code/python/uv-script.py"
```

Et ensuite :

```shell
chmod 755 uv-script.py
./uv-script.py # (1)!
```

1. Le nom du fichier n’est bien évidemment pas significatif.

C’est vraiment un des avantages de `uv` de pouvoir se passer de la partie virtualenv dans certains contextes, notamment pédagogiques.


## :material-nintendo-game-boy: Pour jouer

:hatching_chick: 2022-11

* [py-rates](https://py-rates.fr/)
* [Pyxel: A retro game engine for Python](https://github.com/kitao/pyxel)
* [PyGame](https://www.pygame.org/)
