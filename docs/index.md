# Accueil

J’avais envie de partager des explorations techniques et des bouts 
de code que je diffusais auparavant via des gists sur Microsoft Github. 
Inspiré par 
[Simon Willison](https://til.simonwillison.net/), [Jeremy Keith](https://adactio.com/) et [Chris Ferdinandi](https://gomakethings.com/articles/),
j’ai trouvé beaucoup de valeur à ces petits partages techniques 
sans prétention. Cela me sert aussi beaucoup d’aide mémoire 👴.

Pour l’instant, vous pouvez naviguer dans ces sections :

<div class="grid cards" markdown>

-   [:fontawesome-brands-python:{ .lg .middle } __Python__](python.md)

    ---

    Parce que j’apprécie ce langage et sa communauté.

    - [Généralités :material-arrow-right-box:](python.md)
    - [Serveurs :material-arrow-right-box:](python-servers.md)

-   [:material-language-javascript:{ .lg .middle } __JavaScript__](javascript.md)

    ---

    Parce que je suis quand même un développeur web.

    - [Généralités :material-arrow-right-box:](javascript.md)
    - [Formulaires :material-arrow-right-box:](javascript-forms.md)
    - [Images :material-arrow-right-box:](javascript-images.md)
    - [Fetch :material-arrow-right-box:](javascript-fetch.md)
    - [Web Components :material-arrow-right-box:](javascript-webcomponents.md)

- [:fontawesome-brands-css3:{ .lg .middle } __CSS__](css.md)
  parce que plus rien n’est simple dans ce bas monde.
- [:fontawesome-brands-html5:{ .lg .middle } __HTML__](html.md)
  parce qu’on n’est plus à un copier-coller près.
- [:octicons-tools-16: __Makefile__](makefile.md)
  parce que je suis de plus en plus fan de cet outil.
- [:material-powershell: __Shell__](shell.md)
  parce qu’il faut bien quelques raccourcis.
- [:material-head-cog: __RegExp__](regexp.md)
  parce qu’il faut bien se prendre la tête des fois.
- [:octicons-database-16: __SQL__](sql.md)
  parce que je suis complètement incompétent.
- [:material-web: __Pages__](pages.md)
  parce que j’ai souvent besoin de publier des choses vite fait.
- [:material-white-balance-incandescent: __Divers__](divers.md)
  parce que je ne sais plus trop où tout catégoriser.

</div>

Il y a aussi une page de [Colophon](colophon.md) qui détaille les outils pour 
construire cet espace.

Les bouts de code sont sous licence MIT (à part précisé autrement)
mais sont surtout le fruit d’un effort collectif entre reprise de 
solutions StackOverflow, analyse de code sur les diverses forges 
disponibles et/ou projets passés…

<div class="termy">

```console
$        Art is pointless    without passion. 
$      You have to go out    and create art. 
$       Get an actual job    doing what you love 
$       and make a living    by being yourself. 
$      You can’t just let    other people define 
$   the rest of your life    and say you will
$   be a joke, a failure.    Follow your heart. 
$         You will end up    happy and free, not 
$      a starving artist.    Love your art and 
$   contribute to society    by inspiring people 
$ instead of wasting time    letting others tell you 
$        you’re worthless.   You can change the world.
```

</div>

==Bonne balade ! :material-hand-wave:==
