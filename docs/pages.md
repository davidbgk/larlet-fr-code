# Pages

J’utilise de plus en plus [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/#gitlab-pages) (et ses dérivés, par exemple [Framagit](https://framagit.org/)).
C’est moins intuitif que ce que j’ai pu expérimenter avec Microsoft Github Pages, aussi voici quelques configurations de base et astuces collectées/expérimentées ici et là car je me perds régulièrement dans la documentation officielle.


## :simple-html5: Servir un dossier qui contient du HTML

:hatching_chick: 2022-11

Il faut commencer par créer un dépôt puis par mettre tous les fichiers HTML (et autres !) dans un dossier `public/`.
Ensuite, on ajoute le fichier `.gitlab-ci.yml` à la racine du dépôt.

!!! Warning Attention

    Il faut bien un point `.` au début du nom du fichier !

```yaml title=".gitlab-ci.yml" linenums="1"
--8<-- "code/pages/gitlab-ci-html.yml"
```

1. Vous pouvez changer cette ligne si vous ne mettez pas tous vos fichiers dans le dossier `public/`.
2. Ici on restreint le déploiement à la branche principale (en général `master` ou `main`).

Une fois tout cela commité/pushé sur le dépôt, vous devriez pouvoir accéder à :

`https://NOM-DU-COMPTE.gitlab.io/NOM-DU-DEPOT/`

Si vous ne voyez rien apparaître au bout de quelques secondes/minutes, vous pouvez aller voir le détail des erreurs directement au niveau du dépôt dans `CI/CD → Pipelines`.


## :simple-speedtest: Améliorer les performances avec gzip

:hatching_chick: 2022-11

Je découvre [grâce à Magentix](https://www.magentix.fr/blog/heberger-gratuitement-un-site-statique.html#gitlab) que les fichiers statiques ne sont pas servis par défaut en étant gzipés.

_Je dois avouer que je n’ai pas trop regardé les performances sur ces environnements car en général je m’en sers uniquement pour des prototypes._

Il est possible de convertir les fichiers statiques lors du déploiement avec l’ajout du script suivant :

```yaml title=".gitlab-ci.yml" linenums="1"
--8<-- "code/pages/gitlab-ci-gzip.yml"
```

!!! info Info

    Notez que vous pouvez adapter la liste des extensions concernées dans la commande (ici le `.html`, `.js` et `.css`). 


## :fontawesome-brands-python: Générer les pages avec Python

:hatching_chick: 2022-11

Bien souvent on ne veut pas commiter directement les pages en HTML mais plutôt se servir de l’intégration continue pour les générer.

La configuration ci-dessous utilise un générateur de site statique en Python pour générer les pages HTML à partir de fichiers _markdown_ qui sont eux ajoutés au dépôt.
On peut imaginer pas mal d’autres usages, ce n’est qu’un exemple.

```yaml title=".gitlab-ci.yml" linenums="1"
--8<-- "code/pages/gitlab-ci-python.yml"
```

1. On installe les dépendances avec `pip` (ou tout autre système)
2. On lance la commande de `build` qui est gérée dans un `Makefile` et qui produit les fichiers HTML dans le dossier `public/` (ou modifier la ligne en-dessous)
3. On ne déploie que la branche principale (`main` en général de nos jours)

On utilise un cache pour `pip` afin de rendre les futurs déploiements plus rapides.


## :octicons-versions-16: Déployer une version du site par branche

:hatching_chick: 2023-02

C’est la suite de la configuration ci-dessus, parfois on veut pouvoir déployer une branche pour des tests tout en gardant la branche principale/de production à la racine de nos Gitlab Pages.

Ce script de déploiement va déployer la nouvelle branche poussée (et les futurs _commits_) sur :

`https://<username>.gitlab.io/<repository>/<branch-name>/`

(En plus de conserver `https://<username>.gitlab.io/<repository>/` pour la branche principale.)

!!! warning

    Chaque déploiement d’une nouvelle branche va écraser une précédente branche déployée (tout en **re**déployant la branche principale).
    Chaque _commit_ sur votre branche principale va aussi supprimer le déploiement de la branche précédemment mise à jour.
    Ce n’est peut-être pas ce que vous souhaitez !

    Il est possible de faire des choses plus avancées avec des _patterns_ de noms de branches pertinentes par exemple à mettre dans la clé `rules`.
    À vous de voir ce qui est compatible avec votre fonctionnement.


```yaml title=".gitlab-ci.yml" linenums="1"
--8<-- "code/pages/gitlab-ci-branches.yml"
```

1. Un exemple de personnalisation de l’environnement (couplé à [dotenv](https://pypi.org/project/python-dotenv/) c’est plus propre), c’est optionnel mais c’est pour donner une idée
2. La commande de construction doit accepter un paramètre lui indiquant où générer le site
3. On revient maintenant sur la branche principale pour déployer à la racine

PS : mettre en cache les installations via `apt-get` est non trivial, j’ai passé un bon moment avant d’abandonner… la recommandation officielle étant de faire sa propre image Docker.

!!! note "Mise à jour (octobre 2024)"

    C’est dorénavant possible nativement avec la version payante de Gitlab, voir
    [la documentation à ce sujet](https://docs.gitlab.com/ee/user/project/pages/#parallel-deployments).


## :octicons-image-16: Générer des fichiers `.webp` à partir de fichiers `.jpg`

:hatching_chick: 2023-02

Ceci n’est pas vraiment une recette mais une inspiration, ça dépend ensuite vraiment de ce que vous voulez faire des images générées !
C’est à adapter aussi si vous avez du `png`, etc.

```yaml title=".gitlab-ci.yml" linenums="1"
--8<-- "code/pages/gitlab-ci-webp.yml"
```

1. On installe les dépendances système
2. On installe l’utilitaire Python [webp-converter](https://github.com/Jacksgong/webp-converter)
3. On crée une arborescence dans `webp/` avec les fichiers `jpg` depuis le dossier `static/`
4. On convertit sur place cette nouvelle arborescence pour avoir des fichiers `webp`

Il est possible de modifier le ratio de qualité en fonction de vos besoins, on peut descendre assez bas selon les cas mais ça finit par ternir les images.

C’est à vous ensuite de mettre cette arborescence là où c’est pertinent pour votre produit.

!!! info "Pro-tip!"
    Toujours mettre des options en version étendue dans les scripts (d’intégration continue).


## :octicons-blocked-16: Bloquer le déploiement (ou les tests) avec un mot-clé

:hatching_chick: 2023-04

!!! Warning "Mise à jour (juillet 2024)"

    Depuis, j’ai découvert qu’en mettant `[skip ci]` dans un message de commit,
    Gitlab se chargeait de ne pas lancer les jobs de la CI. La méthode ci-dessous
    reste valable si vous voulez du plus granulaire.


Vous voulez parfois ne pas exécuter l’une des commandes définies dans votre intégration continue (typo qui ne nécessite pas un redéploiement, fichier de configuration qui n’impacte pas les tests, etc).

```yaml title=".gitlab-ci.yml" linenums="1"
--8<-- "code/pages/gitlab-ci-nopages.yml"
```

1. On ajoute une règle pour que le lancement ne s’effectue que lorsque le message (titre ou description) de _commit_ ne contient pas (`!~`) le mot-clé « nopages ».


Lorsqu’il s’agit d’une tâche qui ne comporte pas de `rules`, c’est encore plus facile/explicite avec le bout de configuration suivant :

```yaml title=".gitlab-ci.yml" linenums="1"
--8<-- "code/pages/gitlab-ci-notest.yml"
```

1. La combinaison du `except` et du `=~` permet de **ne pas** lancer la tâche lorsque le message (titre ou description) de _commit_ contient le mot-clé « notest ».


!!! eco "Préservation des ressources"
    Vous avez beau squatter les ressources matérielles des autres, ça n’en reste pas moins des CPU qui tournent de l’autre côté de la planète et qui réchauffent de l’eau (au mieux). Avoir un moyen de passer outre cette consommation continue est quand même pas mal.


## :octicons-git-pull-request-16: Commiter des fichiers générés par la CI

:hatching_chick: 2024-07

J’ai passé un temps non négligeable pour réussir à faire tourner un script qui génère
des fichiers que je voulais ensuite ajouter au dépôt. Voici le code complet auquel je
suis arrivé, c’est plus qu’il n’en faut mais ça peut donner d’autres idées :

```yaml title=".gitlab-ci.yml" linenums="1"
--8<-- "code/pages/gitlab-ci-commit.yml"
```

On essaye de réutiliser au maximum les variables d’environnement qui sont déjà
accessibles dans la CI de Gitlab. Il faut néanmoins créer un *token* personnel
(à moins d’avoir un compte payant) ==qui ne peut avoir une durée de vie de plus d’un an==
puis aller ajouter une variable d’environnement que j’ai intitulée `CI_TOKEN_NAME_VALUE`
et qui contient `nom-du-token:glpat-suivi-par-un-id`. Je recommande de mettre
cette variable en *Masked* et à vous de voir pour l’option *Protected* selon
votre gestion des branches.

On s’assure de sortir du script s’il n’y a pas de nouveaux fichiers générés et
on ajoute le mot-clé `[skip ci]` dans le message de commit sinon on fait tourner
la CI en boucle vu que le job est déclenché au commit !

!!! Warning Attention

    Si vous voulez uniquement mettre à jour des fichiers déjà versionnés mais
    _ne pas_ ajouter de nouveaux fichiers générés (et/ou artefacts de CI),
    il faudra adapter les commandes `git` (option `--untracked-files=no`).
    Il est aussi possible de jouer avec le `.gitignore` pour ça.

## :material-web-sync: Utiliser une clé SSH depuis la CI Gitlab

:hatching_chick: 2024-10

!!! info "Préalable"

    Il faut avoir [généré une clé SSH](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair) sur sa machine (sans mot de passe…) et avoir mis la clé **publique** sur la machine vers laquelle on veut communiquer.
    Puis saisir deux variables d’environnement pour votre CI Gitlab :

    1. `SSH_PRIVATE_KEY` de type `File` (très important !) qui contient le contenu de la clé **privée** avec une ligne vide à la fin (très important aussi !)
    2. `SSH_KNOWN_HOSTS` de type `File` qui contient le résultat de `ssh-keyscan -t ed25519 host-ou-nom-de-domaine` vers lequel vous voulez communiquer (toujours avec une ligne vide à la fin)

```yaml title=".gitlab-ci.yml" linenums="1"
--8<-- "code/pages/gitlab-ci-ssh.yml"
```

Le plus difficile est de ne pas se louper sur les droits… ni sur les variables de la CI.
Rassurez-moi, vous aussi vous avez besoin d’au moins 10 pushs pour mettre une CI sur pieds ?!

!!! warning

    Il y a des erreurs dans la [doc officielle](https://docs.gitlab.com/ee/user/ssh.html) et dans la plupart des tutoriels sur lesquels je me suis cassé les dents…
