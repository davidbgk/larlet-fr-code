---
date: 2023-12-26
---

# uMap et Web Components

En ce moment, je m’amuse beaucoup avec les [Web Components](/javascript-webcomponents/) (page dédiée locale) et comme [uMap](https://github.com/umap-project/umap/) (github.com) est mon terrain de jeu actuel je n’ai pas résisté à tenter des choses en « production » avec. Petit retour sur ces premiers pas.

<!-- more -->

Tout d’abord, les [Web Components](https://developer.mozilla.org/fr/docs/Web/API/Web_Components) (mdn.org) regroupent plusieurs concepts et spécification. Il y a (et va y avoir) probablement autant de façon d’utiliser des composants web que ce qu’il y a des *frameworks* aujourd’hui même si ça standardise / force quelques pratiques.

Ce que je trouve d’attirant — outre la *hype* — c’est de pouvoir enrichir progressivement du HTML et c’est ce que j’ai tenté pour [umap-project.org](https://umap-project.org/). Si on prend par exemple le composant `#!html <stargazers-count>900</stargazers-count>`, il contient un chiffre approximatif affiché au chargement même pour les navigateurs ne sachant pas interpréter des *custom elements* et ensuite, une fois le composant éventuellement chargé, on va pouvoir aller récupérer la valeur actuelle de manière dynamique sur l’API de Microsoft GitHub :

```js title="web-components.js" linenums="1"
class StargazersCount extends HTMLElement {
  static register(tagName = 'stargazers-count') {
    if ('customElements' in window && !customElements.get(tagName)) {
      customElements.define(tagName, this)
    }
  }

  connectedCallback() {
    fetch('https://api.github.com/repos/umap-project/umap') // (1)!
      .then((response) => {
        if (response.ok) { // (2)!
          return response.json()
        }
        throw response
      })
      .then((data) => (this.textContent = data.stargazers_count)) // (3)!
      .catch((error) => console.debug(error)) // (4)!
  }
}

StargazersCount.register()
```

1. URL de l’API pour récupérer cette valeur.
2. Toujours vérifier que la réponse est OK car `fetch` ne le fait pas pour vous.
3. C’est ici que l’on remplace la valeur initiale avec le décompte à jour.
4. Si erreur en ligne 14, elle se retrouve dans la console en `debug`.

*Et voilà*, en une vingtaine de lignes de JS on a une mise à jour asynchrone de notre valeur.
Aucune autre dépendance externe que celle de l’appel à l’API n’a été nécessaire.
Aucun *framework* et autre réactivité pour ce cas trivial.

Il en est de même pour plusieurs autres éléments de la page :

- statistiques d’usage ;
- dernière *release* / *commit*.

À chaque fois, ce sont des données non sensibles et je ne me serais pas permis de faire ainsi s’il était critique d’avoir des valeurs correctes pour tout le monde. Même si niveau [Baseline™](https://developer.mozilla.org/en-US/blog/baseline-evolution-on-mdn/) c’est correct, j’aspire à rendre des sites accessibles à des personnes ayant des périphériques plus mis à jour depuis plus longtemps…

!!! note "TIL: petit bonus"

    Comme il y a de longs chiffres à afficher et [deux](https://umap-project.org/) [langues](https://umap-project.org/fr/), j’ai aussi utilisé l’API `Intl` du navigateur en fonction de la langue de la page `#!html <html lang="fr">` :
    
    ```js linenums="1"
    this.textContent = new Intl.NumberFormat(
      document.querySelector('html').lang
    ).format(data.maps_active_count)
    ```

Dans le prochain épisode… [des cartes accessibles ?](https://umap-project.gitlab.io/leaflet-webcomponents/)
