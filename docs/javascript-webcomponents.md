# Web components et JavaScript


## :fontawesome-solid-puzzle-piece: Un gabarit de Web Component

:hatching_chick: 2023-11

Il y a pas mal de façons de faire et c’est une combinaison de ce qu’on pu partager récemment les influenceurs tech de ma bulle (surtout [Zach Leat](https://www.zachleat.com/) et [Chris Ferdinandi](https://gomakethings.com/)). Merci à [Anthony](https://ricaud.me/blog/) pour la partie sur l’écoute des attributs !

```js title="web-components-template.js" linenums="1"
--8<-- "code/javascript/web-components-template.js"
```

1. Il s’agit d’un exemple avec un `template` dynamique, si le HTML peut être ajouté directement dans le Web Component c’est mieux car plus résilient si le JS plante au chargement par exemple…
2. J’aime bien le fait que le composant puisse s’auto-enregistrer tout en laissant la faculté de personnaliser le nom. C’est optionnel.
3. On définit les attributs dont on veut observer les changements.
4. Ça va automagiquement ajouter un `this.shadowRoot`, voir cet [article détaillé de Lee James Reamsnyder](https://www.leereamsnyder.com/web-component-and-somehow-also-js-101). On le met ouvert pour pouvoir jouer avec en JavaScript par la suite.
5. Astuce pour dispatcher les évènements vers des méthodes dédiées. Voir l’[article de Andrea Giammarchi](https://webreflection.medium.com/dom-handleevent-a-cross-platform-standard-since-year-2000-5bf17287fd38) pour comprendre la méthode standard `handleEvent` sur un objet.
6. C’est une bonne pratique de déconnecter les écouteurs rendus inutiles, a fortiori sur des web components !
7. Si l’attribut `color` ou `size` change sur le composant, on passe là-dedans, magique !

Par ici un exemple assez brut car c’est toujours chouette de jouer avec en direct :

[Ouvrir dans un nouvel onglet](examples/web-component.html){ .md-button target="_blank" }


## :material-timer-sand: Chargement des Web Components

:hatching_chick: 2022-08

Les Web Components étant chargés en JavaScript, il y a un moment où il
peut y avoir un flash lors du chargement.
Cette astuce [proposée par Cory LaViska](https://www.abeautifulsite.net/posts/flash-of-undefined-custom-elements/) permet de donner une
impression de chargement synchrone à la page.

Plutôt que de le faire en CSS :

```css linenums="1"
:not(:defined) {
  visibility: hidden;
}
```

On va le faire en JavaScript (combiné à du CSS) :

```html title="web-components-loading.html" linenums="1"
--8<-- "code/html/web-components-loading.html"
```

On attend que chacun des composants soit chargé pour afficher le
`#!html <body>` avec une transition sur l’opacité. Selon la portée de
vos composants, il n’est pas forcément nécessaire d’appliquer cela à
la page entière mais ça donne des idées.


## :simple-stylelint: Styler des Web Components

:hatching_chick: 2022-11

Il est possible d’appliquer des styles à un composant web directement avec des variables CSS et Manuel Matuzovic [nous le démontre](https://www.matuzo.at/blog/2022/100daysof-day28/) :


```js title="web-components-styling.js" linenums="1" hl_lines="9 10 12"
--8<-- "code/javascript/web-components-styling.js"
```

On peut ensuite l’utiliser ainsi par défaut :

```html linenums="1"
<custom-alert>
  Votre demande est bien arrivée sur nos serveurs. 😊
</custom-alert>
```

Et ajouter une classe avec les bons styles — hors du web component — pour un autre type de messages :

```html linenums="1"
<custom-alert class="error">
  Votre demande était un peu trop cavalière ! 🐎
</custom-alert>
```

```css linenums="1"
.error {
  --alert-bg: rgb(255 119 119);
  --alert-spacing: 2rem;
}
```

Stylé !


## :simple-stylelint: Styler des Web Components, encore

:hatching_chick: 2023-11

Tiré et adapté d’[un article](https://www.zachleat.com/web/w3c-banner-web-component/) et de son [dépôt associé](https://github.com/zachleat/w3c-banners).

```js title="web-components-styling-suite.js" linenums="1"
--8<-- "code/javascript/web-components-styling-suite.js"
```

1. Oui c’est bien une variable CSS qui définit une autre variable CSS pour ajouter du dynamisme. _Neat!_
2. On coupe la moutarde sur `replaceSync` ou si les styles sont déjà appliqués.
3. Combinaison de `CSSStyleSheet` + `replaceSync` + `adoptedStyleSheets`, wow. Sympa comme découverte.


## :call_me: Des évènements pour les Web Components

:hatching_chick: 2023-12

Une manière générique d’émettre des évènements, inspiré par ce que propose [Chris Ferdinandi](https://gomakethings.com/custom-events-in-web-components/) :

```js title="web-components-events.js" linenums="1"
--8<-- "code/javascript/web-components-events.js"
```

1. C’est cette méthode qui est réutilisable et qui envoie des évènements relativement standardisés.
2. On l’utilise ainsi en passant ce que l’on veut dans l’objet.
3. On l’écoute ensuite, par exemple dans un autre composant web.


## :fontawesome-solid-wand-magic-sparkles: Des mixins pour les Web Components

:hatching_chick: 2023-12

Si vous reprenez deux des astuces précédentes pour l’enregistrement et les évènements, il peut être intéressant de ne pas avoir à les répéter pour chaque définition de composant !

Ici on crée des _mixin_ qui vont permettre d’étendre le comportement par défaut de `HTMLElement`.
Merci à [Luke Secomb](https://blog.lukesecomb.digital/article/lwc-custom-mixins).

```js title="web-components-mixins.js" linenums="1"
--8<-- "code/javascript/web-components-mixins.js"
```


## :octicons-plug-16: Laisser le choix de définir un Web Component

:hatching_chick: 2024-10

Une idée de [Nathan Knowler](https://knowler.dev/blog/to-define-custom-elements-or-not-when-distributing-them) qui utilise un paramètre dans l’URL du fichier JS pour charger ou pas le Custom Element par défaut. Malin !

```js title="web-components-loading.js" linenums="1"
--8<-- "code/javascript/web-components-loading.js"
```

Ce qui permet ensuite de charger le composant avec une auto-définition :

```html
<script type=module src=./web-components-loading.js?define></script>
```

… ou le définir à sa sauce, avec son propre nom :

```html
<script type="module">
  import DefinedElement from './web-components-loading.js'
  customElements.define('super-element', DefinedElement)
</script>
```


## :material-download: Charger dynamiquement des Web Components

:hatching_chick: 2024-10

Une astuce de [Chris Ferdinandi](https://gomakethings.com/how-to-dynamically-load-web-components/) pour ne pas charger des Custom Elements qui ne sont même pas utilisés :

```js title="web-components-dynamic.js" linenums="1"
--8<-- "code/javascript/web-components-dynamic.js"
```

Simple et efficace (vérifier le [support de `import()`](https://caniuse.com/es6-module-dynamic-import) pour votre convenance bien sûr).

## :fontawesome-solid-wand-sparkles: Un innerHTML moderne avec évaluation du shadow DOM

:hatching_chick: 2024-10

Une nouvelle méthode découverte en explorant le code derrière [cet article](https://www.spicyweb.dev/action-web-components/) qui permet de faire en sorte que le [declarative shadow DOM soit interprété](https://thathtml.blog/2024/01/dsd-safety-with-set-html-unsafe/) depuis une chaîne de texte renvoyée par le serveur :

```js title="set-html-unsafe-dsd.js" linenums="1"
--8<-- "code/javascript/set-html-unsafe-dsd.js"
```

C’est du [baseline 2024](https://developer.mozilla.org/en-US/docs/Web/API/Element/setHTMLUnsafe) mais si vous travaillez avec des Web Components + shadow DOM c’est plutôt essentiel.

Ça peut s’utiliser ensuite ainsi :

```javascript
const fetchAndProcess = async (path, elOrSelector) => {
  const partial = await fetchPartial(path)
  const el =
    (typeof elOrSelector === "string")
      ? document.querySelector(elOrSelector)
      : (elOrSelector || document.body)
  el.append(...partial.children)
}
```

## :material-walk: Aller plus loin/différemment

* [Add Responsive-Friendly Enhancements to &lt;details> with &lt;details-utils>](https://www.zachleat.com/web/details-utils/)
* [Web Components as Progressive Enhancement](https://cloudfour.com/thinks/web-components-as-progressive-enhancement/)
* [Building tabs in Web Components](https://darn.es/building-tabs-in-web-components/)
* [Nude UI: A collection of accessible, customizable, ultra-light web components](https://nudeui.com/)
* [Shoelace: A forward-thinking library of web components.](https://shoelace.style/)
* [Lego](https://lego.js.org/)
