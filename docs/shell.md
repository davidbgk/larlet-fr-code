# Shell

J’utilise actuellement `zsh` qui est présent de base dans macOS.
Je n’ajoute que très peu de scripts pour rester aussi proche de
l’environnement de personnes débutantes que je pourrais accompagner.

Vous pouvez ajouter ces lignes dans votre fichier `~/.zshrc` et
ça ne devrait pas être très éloigné de la syntaxe des autres shells.

## :material-bike-fast: Alias

:hatching_chick: 2022-08

Je me contente de ceux-ci présentement, c’est principalement 
pour gérer mes environnements Python plus facilement :

```shell title="aliases.sh" linenums="1"
--8<-- "code/shell/aliases.sh"
```

Je sais que les CoolKids® utilisent Poetry ou Pyenv ou que sais-je
encore mais je crois que je préfère encore savoir ce qu’il se passe
sur ma machine.


## :material-account-reactivate: Auto dé·activation des virtualenvs

:hatching_chick: 2022-08

Script permettant d’activer (ou de désactiver) automagiquement un
`virtualenv` Python lorsqu’on fait un `#!shell cd` dans un dossier qui en
possède un.

```shell title="activation-venv.sh" linenums="1"
--8<-- "code/shell/activation-venv.sh"
```

1.  Au passage, on ajoute `node_modules/.bin` au `$PATH`.


## :material-code-json: jq mais sans jq

:hatching_chick: 2022-10

Je suis toujours _fan_ de ces “You might not need…”, cette fois-ci c’est pour se passer de [jq](https://stedolan.github.io/jq/) et c’est [Üllar Seerme](https://usrme.xyz/tils/you-might-not-need-jq-for-simpler-use-cases/) qui nous le propose :

```shell linenums="1"
jq() {
    echo -n "$1" | python3 -c "import json, sys; print(json.load(sys.stdin)${2})"
}
```

Cela permet par exemple de récupérer/traiter une chaîne ou arborescence simple dans un fichier JSON sans autre dépendance que Python :

<div class="termy">

```console
$ jq "$(curl --silent https://github.com/davidbgk.json)" "['message'].split(',')[0]"
Hello there
```

</div>

Aussi, [jc](https://github.com/kellyjonbrazil/jc) permet de convertir des commandes shell en JSON et c’est aussi un paquet Python :yum:.


## :seedling: Un template pour script bash

:hatching_chick: 2022-10

Issu de [cet article](https://sharats.me/posts/shell-script-best-practices/) de Shrikant Sharat Kandula qui donne d’autres astuces pour faire des scripts résilients.

```shell title="template.sh" linenums="1"
--8<-- "code/shell/template.sh"
```

1. On veut sortir en cas d’erreur
2. On veut sortir si une variable est manquante
3. On veut considérer l’erreur si elle intervient dans une suite de commandes pipe-eés
4. Très utile pour le _debug_ avec `TRACE=1 ./script.sh`
5. Ne pas oublier l’aide et une façon d’accepter des arguments courts et longs
6. On se déplace dans le dossier d’execution du script, c’est souvent souhaitable
7. C’est ici pour ajouter son code !


## :simple-aiohttp: Chercher par quoi est occupé un port

:hatching_chick: 2024-06


Via [Bill Mill + Julia Evans](https://notes.billmill.org/computer_usage/network/list_all_listening_ports_on_a_mac.html).

```shell linenums="1"
sudo lsof -iTCP -sTCP:LISTEN -iUDP -n -P
```


