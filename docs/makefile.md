# Makefile

## :fontawesome-solid-wand-magic-sparkles: Auto-documentation

:hatching_chick: 2022-08

Il y a de très nombreuses façon de le faire (voir plus bas).
J’apprécie la version Python qui, même si elle est plus verbeuse,
a l’avantage d’être compréhensible avec mes connaissances actuelles.

```make title="Makefile" linenums="1" hl_lines="23-40"
--8<-- "code/makefile/Makefile-autodocumentation.make"
```

1. :material-file-document-edit: On documente les commandes directement dans le Makefile
2. :material-file-document-edit: On documente les commandes directement dans le Makefile
3. :material-file-document-edit: On documente les commandes directement dans le Makefile
4. :material-language-python: Le script Python commence ici et il [vient de là-bas](https://daniel.feldroy.com/posts/autodocumenting-makefiles).

Une fois les commentaires ajoutés au `Makefile`, ils deviennent
accessibles sous forme d’aide lorsqu’on lance la commande `make` :


<div class="termy">

```console
$ make

install    Install the dependencies.
serve      Launch a local server to serve the `public` folder.
test       Launch all tests.
```

</div>

Cette astuce [vient de Daniel Feldroy](https://daniel.feldroy.com/posts/autodocumenting-makefiles).
D’autres articles à ce sujet :

* [Self-Documented Makefile](https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html)
* [make help - Well documented Makefiles](https://www.thapaliya.com/en/writings/well-documented-makefiles/)
* [Self-Documenting Makefiles](https://www.client9.com/self-documenting-makefiles/)
* [Add a help target to a Makefile that will allow all targets to be self documenting](https://gist.github.com/prwhite/8168133)


## :material-language-python: Makefile, Python et venv

:hatching_chick: 2022-11

Il n’est pas toujours évident d’activer l’environnement virtuel Python avant le lancement d’une commande `make`, surtout pour des néophytes et/ou n’évoluant pas dans cet environnement (haha).
Bastian Venthur [propose une version](https://venthur.de/2021-03-31-python-makefiles.html) qui permet de préfixer chaque commande pour éviter d’avoir à activer le `venv` :

```make title="Makefile" linenums="1" hl_lines="15-20"
--8<-- "code/makefile/Makefile-python-venv.make"
```

On s’assure ici avant chaque lancement de commande que l’environnement virtuel Python est créé et à jour.
Puis, on préfixe chaque utilisation des binaires du projet par le chemin du `virtualenv` dans lequel il est contenu.

Voir aussi [cet article de Mathieu Leplatre](https://blog.mathieu-leplatre.info/tips-for-your-makefile-with-python.html) à ce sujet, dont je retiens d’ailleurs l’excellente idée d’afficher un message d’erreur si Python n’est pas trouvé :

```make title="Makefile" linenums="1"
--8<-- "code/makefile/Makefile-python-notfound.make"
```

1. Il est tout à fait possible d’adapter le chemin où vous avez l’habitude de stocker vos environnements virtuels.

## :material-walk: Aller plus loin/différemment

* [Why I Prefer Makefiles Over package.json Scripts](https://spin.atomicobject.com/2021/03/22/makefiles-vs-package-json-scripts/)
* [Learn Makefiles](https://makefiletutorial.com/)
