# JavaScript

## :material-clock-fast: Raccourci pour sélectionner une liste de nœuds

:hatching_chick: 2022-08

La méthode standard qui consiste à utiliser `#!js querySelectorAll()`
est relativement verbeuse et ne permet pas de faire facilement un
`#!js forEach()` dessus. Cette version simplifiée est un raccourci utile
lorsqu’on doit le faire souvent :

```js title="qsa.js" linenums="1"
--8<-- "code/javascript/qsa.js"
```

1.  On en profite pour retourner un `#!js Array` !


## :fontawesome-solid-anchor-circle-check: Récupérer la chaîne de l’ancre d’une URL

:hatching_chick: 2022-08

Je passe toujours trop de temps à retrouver comment faire.

```js title="anchor.js" linenums="1"
--8<-- "code/javascript/anchor.js"
```


## :material-emoticon-sick-outline: Prendre en compte les préférences utilisateur·ice pour lancer une animation

:hatching_chick: 2022-08

Très important car des personnes sont mal à l’aise avec certaines 
animations (j’en fait partie, parfois).

```js title="motion.js" linenums="1"
--8<-- "code/javascript/motion.js"
```

Voir aussi ce qu’il est possible de faire [en HTML](html.md#prendre-en-compte-les-preferences-utilisateurice-pour-lancer-une-animation) et [en CSS](css.md#prendre-en-compte-les-preferences-utilisateurice-pour-lancer-une-animation).


## :material-calendar-plus: Raccourci pour créer/attacher un évènement

:hatching_chick: 2022-08

Un bout de code qui vient de Chris Ferdinandi
(voir [1](https://gomakethings.com/custom-events-with-vanilla-js/), [2](https://gomakethings.com/a-vanilla-js-custom-event-helper-function/) et [3](https://gomakethings.com/custom-event-naming-conventions/)) :

```js title="emit.js" linenums="1"
--8<-- "code/javascript/emit.js"
```

Il peut s’utiliser ainsi 
`#!js emit('calculator:add', {total: 20}, calendarElement)`
et s’écouter de manière classique.


## :material-bandage: Polyfills à la demande

:hatching_chick: 2022-08

Il est possible d’importer des `polyfills` externes directement 
depuis une balise `#!html <script>`, par exemple pour 
`Promise` ou `fetch` :

```js title="polyfills.html" linenums="1"
--8<-- "code/html/polyfills.html"
```

En pratique, c’est quand même mieux de ne pas faire des requêtes
externes et d’avoir les fichiers en local.


## :material-clipboard-edit: Copier dans le presse-papier

:hatching_chick: 2022-08

Lorsqu’il y a un champ avec un code ou une URL à copier-coller, ça peut
être intéressant de proposer à l’utilisateur·ice de cliquer pour mettre
l’information dans son presse-papier et le coller ailleurs.

```js title="copy-clipboard.js" linenums="1"
--8<-- "code/javascript/copy-clipboard.js"
```

1. Vous pouvez adapter cette valeur qui correspond au temps d’affichage
   de l’information (en millisecondes).


## :material-delete-alert: Confirmation de suppression

:hatching_chick: 2022-08

La façon la plus simple d’ouvrir une popup de confirmation.

```js title="delete-confirmation.js" linenums="1"
--8<-- "code/javascript/delete-confirmation.js"
```

!!! danger "Fermeture automatique"
    Notez qu’à force de recevoir des notifications, les utilisateur·ices
    sont promptes à fermer une fenêtre, aussi rouge soit-elle.
    Il peut-être pertinent de demander une confirmation qui demande de
    saisir quelque chose comme le fait Github lorsqu’on supprime un
    dépôt (il faut taper le nom complet du dépôt concerné).


## :octicons-link-16: Générer un slug

:hatching_chick: 2022-08

Pour transformer un titre en français en une portion d’URL par exemple.
Adapté depuis [cet article](https://mhagemann.medium.com/the-ultimate-way-to-slugify-a-url-string-in-javascript-b8e4a0d849e1).

```js title="slugify.js" linenums="1"
--8<-- "code/javascript/slugify.js"
```


## :material-puzzle: Un système de plugins

:hatching_chick: 2022-08

Ce fragment est issu de [ce qu’à fait Simon Willison](https://simonwillison.net/2021/Jan/3/weeknotes/)
pour Datasette, aidé [par Matthew Somerville](https://github.com/simonw/datasette/issues/983#issuecomment-752888552).

```js title="plugins.js" linenums="1"
--8<-- "code/javascript/plugins.js"
```

Il s’utilise ensuite ainsi :

```js linenums="1"
datasette.plugins.register('numbers', ({a, b}) => a + b)
datasette.plugins.register('numbers', o => o.a * o.b)
datasette.plugins.call('numbers', {a: 4, b: 6})
```


## :material-code-braces: Paramètres par défaut

:hatching_chick: 2022-08

Il est possible de définir des paramètres par défaut en utilisant la syntaxe suivante :

```js title="parameters.js" linenums="1"
--8<-- "code/javascript/parameters.js"
```

Il est aussi possible d’avoir un comportement similaires aux `#!python **kwargs` en Python grâce au _rest parameter_ :

```js title="parameters-kwargs.js" linenums="1"
--8<-- "code/javascript/parameters-kwargs.js"
```

Cela nécessite d’appeler la fonction avec un objet (ce qui pourrait être une bonne pratique ?).

```js linenums="1"
checkNameWithKwargs({name: 'Télaïre', tessiture:'soprano'})
```

## :material-download: Télécharger un fichier dynamiquement

:hatching_chick: 2022-09

Parfois, on veut pouvoir télécharger un fichier généré dynamiquement en JavaScript, par exemple ici pour transformer un tableau en un export CSV (la récupération des données n’est pas montrée).

```js title="download-file.js" linenums="1"
--8<-- "code/javascript/download-file.js"
```

1. Le contenu souhaité du fichier à aller récupérer par ailleurs
2. C’est toujours intéressant d’avoir la date dans des fichiers d’export
3. Création d’un Blob simulant le fichier
4. Création du faux lien qui va nous servir à simuler le téléchargement
5. Utilisation de l’attribut `download` qui permet de donner un nom au fichier téléchargé
6. Création d’une URL contenant le contenu du fichier
7. Simulation du clic avant la suppression de l’élément pour lancer le téléchargement


## :material-keyboard-space: Enlever des espaces

:hatching_chick: 2022-09

Il est très courant de vouloir enlever des espaces et sauts de lignes (depuis un `element.textContent` par exemple).

```js title="strip-extra-spaces.js" linenums="1"
--8<-- "code/javascript/strip-extra-spaces.js"
```

## :material-ruler: Avoir une méthode range()

:hatching_chick: 2022-11

Un moyen d’avoir une méthode `range()` qui ait le même comportement qu’en Python, décrite [en détail par Kieran Barker](https://barker.codes/blog/create-a-range-of-numbers-using-the-array-from-method/) et [adaptée par Chris Ferdinandi](https://gomakethings.com/a-vanilla-javascript-range-method/).

```js title="range.js" linenums="1"
--8<-- "code/javascript/range.js"
```

## :material-hand-clap: Avoir une méthode zip()

:hatching_chick: 2022-11

Un vieux truc que j’avais [récupéré sur StackOverflow](https://stackoverflow.com/a/10284006) qui est quasiment un équivalent de la méthode `zip()` en Python.

```js title="zip.js" linenums="1"
--8<-- "code/javascript/zip.js"
```


## :octicons-log-24: Utiliser des loggers personnalisés

:hatching_chick: 2022-11

Il est possible de définir ses propres _loggers_ pour s’y retrouver plus facilement dans la console.
C’est notamment utile si vous faites une lib ou un module réutilisable.

```js title="logger.js" linenums="1"
--8<-- "code/javascript/logger.js"
```

Cela peut s’utiliser ensuite ainsi (très adaptable en fonction de votre sensibilité) :

```js linenums="1"
const log = logger('myapp')
const logerr = logger('myapp', 'error', 'red')
const logdev = logger('myapp', 'debug', 'blue')
```

Et puis dans votre code enfin :

```js linenums="1"
log('page initialized')
logerr('oops')
logdev('wtf')
```

Je vous laisse vous amuser dans la console :).


## :camel: Du camel au kebab

:hatching_chick: 2022-11

Toute petite fonction mais bien pratique pour convertir des noms de variables en `data-attributes` par exemple !

```js title="camel-to-kebab.js" linenums="1"
--8<-- "code/javascript/camel-to-kebab.js"
```

## :material-clock-fast: Raccourcis pour les attributs

:hatching_chick: 2022-11

Et là vous allez comprendre pourquoi la précédente fonction était utile.

```js title="attributes.js" linenums="1"
--8<-- "code/javascript/attributes.js"
```

1. Nécessite la fonction `camelToKebab()` définie juste au-dessus.

Cela s’utilise ainsi :

```js linenums="1"
attr(el, 'foo') // récupère l’attribut `foo` de `el`
attr(el, 'bar', 'baz') // l’attribut `bar` passe à `baz`
attr(el, 'quux', null) // retrait de l’attribut `quux`
attr(el, [ dataFoo: 'valueBar', dataBaz: 'valueQuux' ])
// set l’attribut `data-foo` à `valueBar` et `data-baz` à `valueQuux`
```


## :material-progress-check: Chargement lorsque le DOM est prêt

:hatching_chick: 2022-11

Il y pas mal de façon de le faire mais ça revient un peu toujours au même.
J’aime bien que ce soit explicite et que ça prenne soin de retirer l’écouteur de l’évènement à la fin.

```js title="dom-ready.js" linenums="1"
--8<-- "code/javascript/dom-ready.js"
```


## :octicons-hash-16: Stocker des informations dans l’URL

:hatching_chick: 2023-01

Une façon de stocker des données ou un état dans un hash d’URL [partagée par Scott Antipa](https://www.scottantipa.com/store-app-state-in-urls).

```js title="store-in-url.js" linenums="1"
--8<-- "code/javascript/store-in-url.js"
```

Il faut par contre une bibliothèque de dé·compression en complément, comme [pako](https://github.com/nodeca/pako) (la partie compress/uncompress).


## :octicons-rel-file-path-16: Générer des chemins relatifs pour des fichiers statiques de modules

:hatching_chick: 2023-01

Découvert via [Going Buildless](https://modern-web.dev/guides/going-buildless/es-modules/#referencing-reusable-assets-with-importmetaurl) de Modern Web.

```js title="modules-relative-paths.js" linenums="1"
--8<-- "code/javascript/modules-relative-paths.js"
```

L’utilisation de la valeur un peu magique de `#!js import.meta.url` va permettre de générer une URL relative au fichier JS concerné, ce qui permet de garder les fichiers statiques proches du code.
Intéressant pour une approche composants/modulaire.


## :safety_pin: Échapper le contenu de variables au rendu

:hatching_chick: 2023-01

Découvert en analysant le code d’[un plugin datasette](https://github.com/simonw/datasette-search-all/blob/5b5f1926d7634ae3ed39f7eaf3fa74325e5d842b/datasette_search_all/templates/search_all.html#L73-L106).

```js title="autoescape.js" linenums="1"
--8<-- "code/javascript/autoescape.js"
```

Ça s’utilise ensuite ainsi (en imaginant que `meh` ait été saisi par l’utilisateur·ice) :

```js linenums="1"
const meh = "<script>"
autoescape`foo ${meh} bar`
// => String { "foo &lt;script&gt; bar" }
```


## :material-code-tags: Retirer les tags HTML d’une chaîne

:hatching_chick: 2023-11

Récupéré depuis [cet article](https://phuoc.ng/collection/html-dom/strip-html-from-a-given-text/) (ce site contient beaucoup d’astuces du genre !).

```js title="striptags-template.js" linenums="1"
--8<-- "code/javascript/striptags-template.js"
```

On crée un élément `#!html template` (vs. `#!html div`), ce qui évite d’exécuter les potentiels scripts inconvenants qui pourraient se trouver dans le HTML transmis. Simple et efficace.

La version consistant à utiliser `DOMParser` est intéressante aussi :

```js title="striptags-domparser.js" linenums="1"
--8<-- "code/javascript/striptags-domparser.js"
```


## :simple-curl: Pré-remplir un formulaire avec les paramètres de l’URL

:hatching_chick: 2023-04

Il est souvent pratique de pouvoir faire un lien vers un formulaire en ayant pré-remplis certains champs `/mon-form/?author=Bob&accept=1`.

La solution suivante pré-suppose que chaque input ait un attribut `name`.

```js title="prefill-form.js" linenums="1"
--8<-- "code/javascript/prefill-form.js"
```


## :material-file-search: Rechercher des mots dans un texte

:hatching_chick: 2023-10

Un travail que l’on avait commencé ensemble avec [Anthony](https://ricaud.me/blog/) pour mon blog et que j’ai adapté et réutilisé récemment.

```html title="search-text.html" linenums="1"
--8<-- "code/html/search-text.html"
```

1.  Je documente aussi la logique de mettre les _stop words_ dans le HTML en JSON
    puis de les parser pour les récupérer car c’est la manière la plus performante de faire.

Voir aussi [cette entrée Python](python.md#retirer-les-accents-dune-chaine-de-caracteres) et les suivantes pour avoir la logique côté serveur.


## :fontawesome-solid-file-zipper: Générer un fichier Zip à télécharger

:hatching_chick: 2024-06

Un article de Josh Martin, [Generating ZIP Files with Javascript](https://www.cjoshmartin.com/blog/creating-zip-files-with-javascript) qui utilise la lib [JSZip](https://stuk.github.io/jszip/) et qui propose un exemple concret :

```js title="zip-download.js" linenums="1"
--8<-- "code/javascript/zip-download.js"
```

1. Génération d’un blob depuis le fichier téléchargé.
2. Génération du fichier Zip à la volée.
3. Création du lien de téléchargement avec le bon nom.

_Si vous avez besoin de le faire [côté serveur en Python](python.md#generer-un-zip-de-fichiers-tout-en-memoire)._


## :material-cursor-default-click: Délégation d’évènements via data-attributes

:hatching_chick: 2024-10

Un [pattern documenté par Chris Ferdinandi](https://gomakethings.com/what-can-you-do-with-data-attributes/) où l’on passe le nom de la fonction JS directement au HTML en `data-attribute`, une forme de `onclick=""` plus idiomatique :

```js title="event-delegation-data-attrs.js" linenums="1"
--8<-- "code/javascript/event-delegation-data-attrs.js"
```

En imaginant du HTML sous cette forme :

```html
<button data-click="sayHi">Say Hello!</button>
<button data-click="expand">Show More</button>
<button data-click="login">Log in</button>
```

## :material-walk: Pour aller plus loin/différemment

* [1LOC](https://1loc.dev/)
* [HTML DOM](https://htmldom.dev/)
* [THIS vs. THAT](https://thisthat.dev/)
* [The Vanilla JS Toolkit](https://vanillajstoolkit.com/)
* [Phuoc Nguyen](https://phuoc.ng/latest/)
* [snap.js](https://thescottyjam.github.io/snap.js/)
* [TinyJS](https://github.com/victorqribeiro/TinyJS)
