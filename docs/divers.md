# Divers

## :simple-mastodon: Un style pour mastodon

:hatching_chick: 2023-01

!!! info

    J’utilise maintenant (circa-2024) uniquement [TangerineUI](https://github.com/nileane/TangerineUI-for-Mastodon/) par [@nileane](https://nileane.fr/@nileane) qui fait un boulot de dingue. MERCI ! :orange_heart:

!!! warning

    Travail en cours, une version potentiellement plus à jour est disponible [directement sur mon serveur](https://fedi.larlet.fr/custom.css).


L’avantage d’avoir sa propre instance est de pouvoir ajouter des styles CSS que l’on trouve un peu plus appropriés à son usage et à sa propre conception de la lisibilité.

```css title="mastodon.css" linenums="1"
--8<-- "code/css/mastodon.css"
```

## :octicons-git-compare-24: Accents et fichiers dans git

:hatching_chick: 2023-09

J’avais souvent une erreur avec des noms de fichiers accentués en français sur certains dépôts git. 
J’ai découvert qu’il fallait changer une configuration sous macOS, soit en faisant :

```sh title="git-unicode.sh" linenums="1"
--8<-- "code/shell/git-unicode.sh"
```

1.  À ne faire qu’une seule fois par machine/OS.
2.  Ou recloner le dépôt.

Soit (pas testé) en faisant :

```sh title="git-quotepath.sh" linenums="1"
--8<-- "code/shell/git-quotepath.sh"
```

1.  À ne faire qu’une seule fois par machine/OS.
2.  Ou recloner le dépôt.

Cela devrait afficher les fichiers avec des noms accentués incohérents mais au moins ça ne bloque pas les interactions avec le dépôt.


## :material-folder-download: Ne récupérer qu’un sous-répertoire de git

:hatching_chick: 2024-10

Il est parfois intéressant de ne récupérer qu’un sous-dossier d’un dépôt complet.
Selon la taille du dépôt et son historique ça peut accélerer le traitement
de plusieurs ordres de grandeur !

Par exemple ici on ne va récupérer que le dossier `docs-users` contenu dans le
[dépôt umap](https://github.com/umap-project/umap) qui contient l’intégralité
du code dont on n’a pas besoin pour générer la documentation statique :

```sh title="git-subfolder.sh" linenums="1"
--8<-- "code/shell/git-subfolder.sh"
```

C’est [inspiré d’un gist](https://gist.github.com/smac89/3d1e3cb1f1b425fa9a400c61c51cff24).

!!! note

    Voir aussi [`git sparse-checkout`](https://git-scm.com/docs/git-sparse-checkout).

## :simple-adobefonts: Ressources pour travailler les polices

:hatching_chick: 2024-10

* [FontSquirrel Webfont Generator](https://www.fontsquirrel.com/tools/webfont-generator) : la base.
* [Wakamai Fondue](https://wakamaifondue.com/) pour inspecter une police et ses capacités
* [fonttools](https://github.com/fonttools/fonttools) (Python) le couteau suisse ([tuto](https://clagnut.com/blog/2418/))
* [Glyphhanger](https://github.com/zachleat/glyphhanger) (JS) pour de l’optimisation, construit sur fonttools ([tuto1](https://www.zachleat.com/web/glyphhanger/), [tuto2](https://www.sarasoueidan.com/blog/glyphhanger/), [tuto3](https://www.stefanjudis.com/notes/glyphhanger-a-tool-subset-and-optimize-fonts/))
* [Font Bakery](http://fontbakery.com/) pour analyser les erreurs potentielles
* [Slice](https://github.com/source-foundry/Slice) pour adapter des polices variables et leurs axes
* [Fontimize](https://github.com/vintagedave/Fontimize) ([motivations](https://daveon.design/introducing-fontimize-subset-fonts-to-exactly-and-only-your-websites-used-characters.html)) pour faire un subset à partir de pages HTML réelles

!!! note

    Il est aussi possible de forger les bonnes URL Google Fonts à la main pour récupérer les fichiers sans aucun outil, voir la [description par ici](https://github.com/googlefonts/roboto-flex/issues/367).


## :octicons-accessibility-inset-16: Ressources pour travailler l’accessibilité

:hatching_chick: 2024-10

* [a11y.css](https://ffoodd.github.io/a11y.css/) pour pointer les incohérences niveau HTML (avec ses extensions navigateurs)
* [sa11y](https://sa11y.netlify.app/overview/) qui fait sensiblement la même chose (avec beaucoup plus de JS)
* [magentaa11y](https://www.magentaa11y.com/web/) pour aider à produire des checklists avec tests
* [Easy Checks](https://www.w3.org/WAI/test-evaluate/preliminary/) documentation par le W3C
* [WAVE (web accessibility evaluation tool)](https://wave.webaim.org/extension/) des extensions avec des infos
* [Axe](https://www.deque.com/axe/) pareil
* [Tanaguru](https://my.tanaguru.com/) un testeur à partir d’une URL
* [Asqatasun](https://asqatasun.org/) (ex-Tanaguru) pour faire des audits
* [A11Y Style Guide](https://a11y-style-guide.com/style-guide/) une référence avec des entrées par composants
* [WhoCanUse](https://www.whocanuse.com/) pour vérifier les contrastes de couleurs et le rendu selon la vision des personnes
* [RandomA11Y](https://randoma11y.com/) un outil pour générer des couleurs avec des contrastes satisfaisants
* [PageSpeed](https://pagespeed.web.dev/) / LightHouse permettent de mettre en lumière certains soucis de perf / accessibilité aussi
