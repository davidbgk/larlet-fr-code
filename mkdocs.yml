site_name: Code.larlet.fr
site_url: 'https://code.larlet.fr'
site_author: David Larlet
site_description: >-
  Partage de petits bouts de codes utiles et commentés en Python, JS, CSS, etc.
copyright: Site par <a href="https://larlet.fr/david/">David Larlet</a>.
repo_url: https://gitlab.com/davidbgk/larlet-fr-code
edit_uri: edit/main/docs/
site_dir: public
watch:
  - docs
  - code
  - mkdocs.yml
theme:
  language: fr
  font: false # (1)
  name: material
  custom_dir: overrides
  features:
    - content.code.annotate
    - navigation.top
    - toc.integrate
  icon:
    repo: fontawesome/brands/git
  palette:
    # Palette toggle for automatic mode
    - media: '(prefers-color-scheme)'
      toggle:
        icon: material/brightness-auto
        name: Switch to light mode

    # Palette toggle for light mode
    - media: '(prefers-color-scheme: light)'
      scheme: default
      primary: teal
      accent: blue
      toggle:
        icon: material/weather-night
        name: Switch to dark mode

    # Palette toggle for dark mode
    - media: '(prefers-color-scheme: dark)'
      scheme: slate
      primary: teal
      accent: blue
      toggle:
        icon: material/weather-sunny
        name: Switch to light mode

nav:
  - 'index.md'
  - 'Python':
      - 'Généralités': 'python.md'
      - 'Serveurs': 'python-servers.md'
  - 'JavaScript':
      - 'Généralités': 'javascript.md'
      - 'Formulaires': 'javascript-forms.md'
      - 'Images': 'javascript-images.md'
      - 'Fetch': 'javascript-fetch.md'
      - 'Web Components': 'javascript-webcomponents.md'
  - 'css.md'
  - 'html.md'
  - 'makefile.md'
  - 'shell.md'
  - 'regexp.md'
  - 'sql.md'
  - 'pages.md'
  - 'divers.md'
  - 'blog/index.md'
  - 'colophon.md'

extra_javascript:
  - static/javascript/termynal.js
  - static/javascript/termynal-setup.js
extra_css:
  - static/stylesheets/termynal.css
  - static/stylesheets/fonts.css
  - static/stylesheets/extra.css
plugins:
  - blog:
      archive: false
      post_readtime: false
      pagination: false
      draft_if_future_date: true
  - privacy
  - tags
  - search:
      lang: fr
  - git-revision-date-localized:
      type: iso_date
      fallback_to_build_date: true
  # Requires a repository setup.
  - rss:
      abstract_chars_count: -1
      date_from_meta:
        as_creation: 'date'
        as_update: false
        datetime_format: '%Y-%m-%d %H:%M'
      length: 10
      pretty_print: false
extra:
  generator: false
  social:
    - icon: fontawesome/brands/mastodon
      link: https://mastodon.social/@dav
    - icon: material/rss-box
      link: /feed_rss_created.xml

markdown_extensions:
  - toc:
      permalink: ⚓︎
  - abbr
  - attr_list
  - md_in_html
  - admonition
  - pymdownx.details
  - pymdownx.inlinehilite
  - pymdownx.superfences
  - pymdownx.keys
  - pymdownx.mark
  - pymdownx.snippets:
      auto_append:
        - includes/abbreviations.md
  - pymdownx.highlight:
      anchor_linenums: true
  - pymdownx.emoji:
      emoji_index: !!python/name:material.extensions.emoji.twemoji
      emoji_generator: !!python/name:material.extensions.emoji.to_svg
