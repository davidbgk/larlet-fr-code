#
# This file is autogenerated by pip-compile with Python 3.9
# by the following command:
#
#    pip-compile requirements.in
#
babel==2.14.0
    # via
    #   mkdocs-git-revision-date-localized-plugin
    #   mkdocs-material
certifi==2023.11.17
    # via requests
charset-normalizer==3.3.2
    # via requests
click==8.1.7
    # via mkdocs
colorama==0.4.6
    # via mkdocs-material
ghp-import==2.1.0
    # via mkdocs
gitdb==4.0.11
    # via gitpython
gitpython==3.1.40
    # via
    #   mkdocs-git-revision-date-localized-plugin
    #   mkdocs-rss-plugin
idna==3.6
    # via requests
importlib-metadata==7.0.0
    # via
    #   markdown
    #   mkdocs
jinja2==3.1.2
    # via
    #   mkdocs
    #   mkdocs-material
markdown==3.5.1
    # via
    #   mkdocs
    #   mkdocs-material
    #   pymdown-extensions
markupsafe==2.1.3
    # via
    #   jinja2
    #   mkdocs
mergedeep==1.3.4
    # via mkdocs
mkdocs==1.5.3
    # via
    #   mkdocs-git-revision-date-localized-plugin
    #   mkdocs-material
    #   mkdocs-rss-plugin
mkdocs-git-revision-date-localized-plugin==1.2.1
    # via -r requirements.in
mkdocs-material==9.5.2
    # via -r requirements.in
mkdocs-material-extensions==1.3.1
    # via mkdocs-material
mkdocs-rss-plugin==1.9.0
    # via -r requirements.in
packaging==23.2
    # via mkdocs
paginate==0.5.6
    # via mkdocs-material
pathspec==0.12.1
    # via mkdocs
platformdirs==4.1.0
    # via mkdocs
pygments==2.17.2
    # via mkdocs-material
pymdown-extensions==10.5
    # via mkdocs-material
python-dateutil==2.8.2
    # via ghp-import
pytz==2023.3.post1
    # via mkdocs-git-revision-date-localized-plugin
pyyaml==6.0.1
    # via
    #   mkdocs
    #   pymdown-extensions
    #   pyyaml-env-tag
pyyaml-env-tag==0.1
    # via mkdocs
regex==2023.10.3
    # via mkdocs-material
requests==2.31.0
    # via mkdocs-material
six==1.16.0
    # via python-dateutil
smmap==5.0.1
    # via gitdb
urllib3==2.1.0
    # via requests
watchdog==3.0.0
    # via mkdocs
zipp==3.17.0
    # via importlib-metadata
