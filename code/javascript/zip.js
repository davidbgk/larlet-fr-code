function zip(rows) {
  return rows[0].map((_, index) => rows.map((row) => row[index]))
}
// zip([['foo', 'bar'], [1, 2]]) => [['foo', 1], ['bar', 2]]
