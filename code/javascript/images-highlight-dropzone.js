;(function highlightOnDragOrFocus(targetSelector, className) {
  'use strict'

  function addEventListenerMulti(element, events, fn) { // (3)!
    /* https://stackoverflow.com/a/45096932 */
    events
      .split(' ')
      .forEach((event) => element.addEventListener(event, fn, false))
  }

  const target = document.querySelector(targetSelector)
  if (target) {
    addEventListenerMulti(target, 'dragenter dragover focus', (event) => {
      event.stopPropagation()
      event.preventDefault()
      target.classList.add(className) // (1)!
    })
    addEventListenerMulti(target, 'dragleave blur', (event) => {
      event.stopPropagation()
      event.preventDefault()
      target.classList.remove(className) // (2)!
    })
  }
})('canvas', 'active')
