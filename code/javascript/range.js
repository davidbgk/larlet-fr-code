function range(stop, start = 1, step = 1) {
  return Array.from(
    { length: (stop - start) / step + 1 },
    (_, i) => start + i * step
  )
}
// range(5) => [ 1, 2, 3, 4, 5 ]
// range(10, 8) => [ 8, 9, 10 ]
// range(10, 1, 3) => [ 1, 4, 7, 10 ]
