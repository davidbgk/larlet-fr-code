function htmlEscape(html) {
  return html
    .replace(/&/g, '&amp;')
    .replace(/>/g, '&gt;')
    .replace(/</g, '&lt;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#039;')
}

class Safe extends String {}

function safe(s) {
  if (!(s instanceof Safe)) {
    return new Safe(s)
  } else {
    return s
  }
}

const autoescape = (fragments, ...inserts) =>
  safe(
    fragments
      .map((fragment, i) => {
        let insert = inserts[i] || ''
        if (!(insert instanceof Safe)) {
          insert = htmlEscape(insert.toString())
        }
        return fragment + insert
      })
      .join('')
  )
