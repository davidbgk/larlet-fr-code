var datasette = datasette || {}
datasette.plugins = (() => {
  var registry = {}
  return {
    register: (hook, fn) => {
      registry[hook] = registry[hook] || []
      registry[hook].push(fn)
    },
    call: (hook, args) => {
      var results = (registry[hook] || []).map((fn) => fn(args || {}))
      return results
    },
  }
})()
