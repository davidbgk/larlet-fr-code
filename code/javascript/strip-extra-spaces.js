const stripExtraSpaces = (str) => str.replace(/\r?\n|\r|\s\s+/g, '')
