const handlers = {
  sayHi(event) {
    console.log('Hello there!')
  },
  expand(event) {
    // Do stuff...
  },
  login(event) {
    // Also do stuff...
  },
}

document.addEventListener('click', (event) => {
  // Get the data-click value
  const type = event.target.getAttribute('data-click')
  if (!type || !handlers[type]) {
    return
  }

  // Run value-specific code
  handlers[type](event)
})
