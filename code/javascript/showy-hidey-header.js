const useShowyHidey = () => {
  let lastScrollTop = 0
  const navbar = document.querySelector('.navbar')
  const navHeight = getComputedStyle(navbar).getPropertyValue('--nav-height')

  const toggleNavOnScroll = () => {
    const currentScrollTop = document.documentElement.scrollTop
    if (
      currentScrollTop > lastScrollTop &&
      currentScrollTop > Number.parseInt(navHeight)
    ) {
      navbar.classList.add('hide')
    } else if (currentScrollTop < lastScrollTop) {
      navbar.classList.remove('hide')
    }
    lastScrollTop = Math.max(0, currentScrollTop)
  }

  if (navbar) {
    window.removeEventListener('scroll', toggleNavOnScroll)
    window.addEventListener('scroll', toggleNavOnScroll)
  }
}

useShowyHidey()
