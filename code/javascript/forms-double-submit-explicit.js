function disableMultipleSubmits() {
  // by Andrea Giammarchi - WTFPL
  Array.prototype.forEach.call(
    document.querySelectorAll('form[disablemultiplesubmits]'),
    function (form) {
      form.addEventListener('submit', this, true)
    },
    {
      // button to disable
      query: 'input[type=submit],button[type=submit]',
      // delay before re-enabling
      delay: 500,
      // handler
      handleEvent: function (e) {
        var button = e.currentTarget.querySelector(this.query)
        button.disabled = true
        setTimeout(function () {
          button.disabled = false
        }, this.delay)
      },
    }
  )
}
