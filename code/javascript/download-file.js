function downloadAsCSV(event) {
  event.preventDefault()
  const content = 'foo;bar' // (1)!
  const name = 'baz_quux'
  const filename = `export_${new Date().toLocaleDateString()}_${name}.csv` // (2)!
  const csvFile = new Blob([content], { type: 'text/csv' }) // (3)!
  const fakeDownloadLink = document.createElement('a') // (4)!
  fakeDownloadLink.download = filename // (5)
  fakeDownloadLink.href = window.URL.createObjectURL(csvFile) // (6)!
  fakeDownloadLink.style.display = 'none'
  document.body.appendChild(fakeDownloadLink)
  fakeDownloadLink.click() // (7)!
  fakeDownloadLink.remove()
}
