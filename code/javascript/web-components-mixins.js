const SelfRegister = (BaseClass) =>
  class extends BaseClass {
    static register(tagName) {
      if ('customElements' in window && !customElements.get(tagName)) {
        customElements.define(tagName, this)
      }
    }
  }

const WithEvents = (BaseClass) =>
  class extends BaseClass {
    emit(type, detail = {}) {
      const event = new CustomEvent(`${this.localName}:${type}`, {
        bubbles: true,
        cancelable: true,
        detail: detail,
      })
      return this.dispatchEvent(event)
    }
  }

class OldPhone extends WithEvents(SelfRegister(HTMLElement)) {
  connectedCallback() {
    this.emit('connected', { message: 'At last!' })
  }
}

OldPhone.register('old-phone')
