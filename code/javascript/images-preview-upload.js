;(function handleImageSelection(
  inputSelector,
  outputSelector,
  dropzoneSelector,
  errorsSelector
) {
  'use strict'

  const input = document.querySelector(inputSelector)
  const output = document.querySelector(outputSelector)
  const dropzone = document.querySelector(dropzoneSelector)
  const errors = document.querySelector(errorsSelector)

  input.addEventListener(
    'change',
    (event) => {
      event.stopPropagation()
      event.preventDefault()
      handleImages(event.target.files)
    },
    false
  )
  dropzone.addEventListener(
    'drop',
    (event) => {
      event.stopPropagation()
      event.preventDefault()
      // TODO: filter out files not matching the
      // `accept="image/*"` pattern.
      handleImages(event.dataTransfer.files)
      input.files = event.dataTransfer.files // (1)!
    },
    false
  )

  function handleImages(images) {
    Array.from(images).forEach((image) => {
      const isOversized = checkOversize(image.size)
      if (isOversized) return
      displayImage(image)
    })
  }

  function checkOversize(imageSize) {
    const maxSizeKb = Number(input.dataset.maxSizeKb)
    const isOversized = Math.round(imageSize / 1024) > maxSizeKb

    if (isOversized) {
      errors.removeAttribute('hidden')
      errors.innerHTML = `<p>Oversized! ${maxSizeKb}Kb is the limit</p>`
      input.value = ''
      return true
    } else {
      errors.innerHTML = ''
      errors.setAttribute('hidden', 'hidden')
      dropzone.setAttribute('hidden', 'hidden')
      return false
    }
  }

  function displayImage(image) {
    // No need for a FileReader in that case:
    // https://developer.mozilla.org/en-US/docs/Web/API/↩
    // File_API/Using_files_from_web_applications↩
    // #example_using_object_urls_to_display_images
    const figure = document.createElement('figure')

    const img = document.createElement('img')
    img.file = image // Required for future upload, fragile?
    img.src = window.URL.createObjectURL(image)
    img.onload = (event) => window.URL.revokeObjectURL(event.target.src)
    img.alt = 'Image preview'
    figure.appendChild(img)

    const figcaption = document.createElement('figcaption')
    figcaption.textContent = image.name
    figure.appendChild(figcaption)

    output.appendChild(figure)
  }
})('input[type="file"]', 'output', 'canvas', '[role="alert"]')
