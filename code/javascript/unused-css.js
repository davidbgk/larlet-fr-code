;(function () {
  let rulesCounter = 0
  Array.from(document.styleSheets).forEach((stylesheet) => {
    const rules = Array.from(stylesheet.cssRules || [])
    const sheethref = stylesheet.href || 'inline'
    rules.forEach((rule) => {
      if (
        !document.querySelectorAll(rule.selectorText).length &&
        rule.selectorText !== undefined
      ) {
        console.log(`${sheethref}: "${rule.selectorText}" not found.`)
        rulesCounter += 1
      }
    })
  })
  console.log(`${rulesCounter} CSS rules unused on that page.`)
})()
