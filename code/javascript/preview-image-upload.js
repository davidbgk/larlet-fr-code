;(function (win, doc) {
  if (!win.FileReader || !win.addEventListener || !doc.querySelectorAll) {
    // doesn't cut the mustard. (1)
    return
  }
  doc
    .querySelectorAll('input[type="file"][accept="image/*"]')
    .forEach(function (fileInput) {
      fileInput.addEventListener('change', function () {
        var files = fileInput.files
        if (files) {
          files.forEach(function (file) {
            var fileReader = new FileReader()
            fileReader.readAsDataURL(file)
            fileReader.addEventListener('load', function () {
              fileInput.insertAdjacentHTML(
                'afterend',
                '<img src="' + this.result + '">'
              )
            })
          })
        }
      })
    })
})(this, this.document)
