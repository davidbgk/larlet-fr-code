function logger(scope, level = 'log', color = 'green') {
  return (...args) => {
    console[level]('%c%s', `color:${color}`, scope, ...args)
  }
}
