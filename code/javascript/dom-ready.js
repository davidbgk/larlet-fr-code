function onload() {
  // An example of what you might want to do:
  var root = document.documentElement
  root.classList.remove('no-js')

  // Do something!

  // Clean up
  document.removeEventListener('DOMContentLoaded', onload)
}

if (document.readyState != 'loading') {
  onload()
} else {
  document.addEventListener('DOMContentLoaded', onload)
}
