class W3CBanner extends HTMLElement {
  static register(tagName = 'w3c-banner') {
    if ('customElements' in window && !customElements.get(tagName)) {
      customElements.define(tagName, this)
    }
  }

  // (1)!
  static style = `
:host {
  --_wb-color: var(--wb-color, #00599b);
  background: var(--_wb-color);
  display: block;
  padding: 1rem;
}
:host([color="green"]) {
  --wb-color: #00a400;
}
:host([color="red"]) {
  --wb-color: #ff0000;
}
:host([color="black"]) {
  --wb-color: #000;
}
`

  connectedCallback() {
    // (2)!
    if (!('replaceSync' in CSSStyleSheet.prototype) || this.shadowRoot) {
      return
    }
    this.attachShadow({ mode: 'open' })
    // (3)!
    let sheet = new CSSStyleSheet()
    sheet.replaceSync(W3CBanner.style)
    this.shadowRoot.adoptedStyleSheets = [sheet]
    this.shadowRoot.innerHTML = `<slot>Hello</slot>`
  }
}

W3CBanner.register()
