function currentAnchor() {
  return document.location.hash ? document.location.hash.slice(1) : ''
}
