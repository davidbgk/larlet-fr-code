// (1)!
const template = document.createElement('template')
template.innerHTML = `<button>Hello world</button>`

class MyComponent extends HTMLElement {
  static register(tagName = 'my-component') {
    // (2)!
    if ('customElements' in window && !customElements.get(tagName)) {
      customElements.define(tagName, this)
    }
  }

  // (3)!
  static observedAttributes = ['color', 'size']

  constructor() {
    super()
    this.attachShadow({ mode: 'open' }) // (4)!
  }

  handleEvent(event) {
    // (5)!
    this[`on${event.type}`](event)
  }

  onclick(event) {
    // Do something when the component is clicked.
  }

  oninput(event) {
    // Same with the input event.
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true))
    this.addEventListener('click', this)
    this.addEventListener('input', this)
  }

  disconnectedCallback() {
    // (6)!
    this.removeEventListener('click', this)
    this.removeEventListener('input', this)
  }

  // (7)!
  attributeChangedCallback(name, oldValue, newValue) {
    console.log(
      `Attribute ${name} has changed from ${oldValue} to ${newValue}.`
    )
  }
}

MyComponent.register()
