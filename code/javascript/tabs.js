const tabs = document.querySelectorAll('[role="tablist"] [role="tab"]')
const eventListenerCallback = setActiveState.bind(null, tabs)

tabs.forEach((tab) => {
  tab.addEventListener('click', eventListenerCallback)
  tab.addEventListener('keyup', (event) => {
    // (1)!
    if (event.code === 'Enter' || event.code === 'Space') {
      tab.querySelector('label').click()
    }
  })
})

function setActiveState(tabs, event) {
  // (2)!
  tabs.forEach((tab) => tab.setAttribute('aria-selected', false))
  event.target.closest('[role="tab"]').setAttribute('aria-selected', true)
}
