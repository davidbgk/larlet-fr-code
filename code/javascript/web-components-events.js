class Modem56k extends HTMLElement {
  // (1)!
  emit(type, detail = {}) {
    const event = new CustomEvent(`${this.localName}:${type}`, {
      bubbles: true,
      cancelable: true,
      detail: detail,
    })
    return this.dispatchEvent(event)
  }

  connectedCallback() {
    // (2)!
    this.emit('connected', { message: 'At last!' })
  }
}

customElements.define('modem-56k', Modem56k)

class OldPhone extends HTMLElement {
  connectedCallback() {
    // (3)!
    this.addEventListener('modem-56k:connected', (event) => {
      console.log(event.detail)
    })
  }
}

customElements.define('old-phone', OldPhone)
