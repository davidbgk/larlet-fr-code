export class DefinedElement extends HTMLElement {
  constructor() {
    super()
    // Do something.
    console.log('Constructed.')
  }
}

if (new URL(import.meta.url).searchParams.has('define')) {
  customElements.define('defined-element', DefinedElement)
}
