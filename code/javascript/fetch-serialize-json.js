const formdata = new FormData(form)
fetch('/test/thing', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(Object.fromEntries(formdata.entries())),
})
  .then((result) => {
    // do something
  })
  .catch((err) => {
    // fix something.
  })
