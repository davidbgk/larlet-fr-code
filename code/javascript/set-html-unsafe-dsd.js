const fetchPartial = async (path) => {
  const response = await fetch(`/${path}`)
  const html = await response.text()

  const tmpl = document.createElement('template')

  // This is a new API, see: https://thathtml.blog/2024/01/dsd-safety-with-set-html-unsafe/
  // and: https://caniuse.com/mdn-api_element_sethtmlunsafe
  if ('setHTMLUnsafe' in tmpl) {
    tmpl.setHTMLUnsafe(html)
  } else {
    // Fall back on using innerHTML for older browsers.
    tmpl.innerHTML = html
  }

  return tmpl.content
}
