// With the help of Jeremy Keith, I was able to create a fully scalable
// code sample that you can copy-paste into your project.
// It will save the user input value on blur, this includes radio buttons,
// checkboxes and date inputs besides regular text/number inputs.
// The only condition is that you give the form element on your page
// a data-attribute of data-form-topic="foo".
// This code snippet saves the data-attribute as the key to the localStorage,
// and the value of it will be an object with key/value pairs of the
// respective inputs name and value.

// VARIABLE DECLARATIONS
// objects
const savedData = {}
let autocompletedData

// HTML elements
const inputs = document.getElementsByTagName('input')

document.addEventListener('DOMContentLoaded', () => {
  const form = document.querySelector('form')

  if (window.localStorage) {
    if (!form) {
      return
    }

    if (!form.dataset.formTopic) {
      return
    }

    const getFormTopic = localStorage.getItem(form.dataset.formTopic)
    if (!getFormTopic) {
      return
    }
    autocompletedData = JSON.parse(getFormTopic)

    const formTopic = form.dataset.formTopic
    console.log(formTopic)

    function getKeyValue() {
      for (const dataKey in autocompletedData) {
        const value = autocompletedData[dataKey]

        let formField = document.querySelector(`[name='${dataKey}']`)

        switch (formField.type) {
          case 'radio':
            formField = document.querySelector(
              `input[name='${dataKey}'][value='${value}']`
            )
            formField.setAttribute('checked', 'checked')
            break
          case 'checkbox':
            formField.setAttribute('checked', 'checked')
            break
          case 'file':
            break
          default:
            formField.value = value
        }
      }
    }

    getKeyValue()
  }
})

if (window.localStorage) {
  function saveFormDataToLocalStorage(e) {
    const form = e.target.closest('form')
    const submitData = new FormData(form)

    for (const [dataKey, value] of submitData.entries()) {
      savedData[dataKey] = value
      console.log(dataKey, value)
    }

    window.localStorage.setItem(form.dataset.formTopic, JSON.stringify(savedData))
  }

  Array.prototype.forEach.call(inputs, (input) => {
    switch (input.type) {
    }

    input.addEventListener('blur', (e) => {
      e.preventDefault()
      saveFormDataToLocalStorage(e)
    })
  })
}
