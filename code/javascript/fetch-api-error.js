fetch('https://jsonplaceholder.typicode.com/tododos')
  .then(function (response) {
    if (response.ok) {
      return response.json()
    }
    throw response
  })
  .then(function (data) {
    console.log(data)
  })
  .catch(function (error) {
    // Check if the response is JSON or not
    let isJSON = error.headers
      .get('content-type')
      .includes('application/json')

    // If JSON, use text(). Otherwise, use json().
    let getMsg = isJSON ? error.json() : error.text()

    // Warn the error and message when it resolves
    getMsg.then(function (msg) {
      console.warn(error, msg)
    })
  })
