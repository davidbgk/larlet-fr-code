for (const [key, value] of new URL(window.location.href).searchParams) {
  const elements = document.getElementsByName(key)
  for (const element of elements) {
    if (['checkbox', 'radio'].includes(element.type)) {
      if (element.value == value) {
        element.checked = true
      }
    } else if (element.multiple) {
      for (const option of element.options) {
        if (option.value == value) {
          option.selected = true
        }
      }
    } else {
      element.value = value
    }
  }
}
