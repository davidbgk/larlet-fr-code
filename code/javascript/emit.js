function emit(type, detail, elem = document) {
  let event = new CustomEvent(type, {
    bubbles: true,
    cancelable: true,
    detail: detail,
  })
  return elem.dispatchEvent(event)
}
