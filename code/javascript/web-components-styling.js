class Alert extends HTMLElement {
  constructor() {
    super()
    this.attachShadow({ mode: 'open' })

    const styles = document.createElement('style')
    styles.textContent = `
      div {
        background-color: var(--alert-bg, rgb(136 177 255 / 0.5));
        color: var(--alert-color, rgb(0 0 0));
        font-weight: bold;
        padding: var(--alert-spacing, 1rem);
      }
    `

    const content = document.createElement('div')
    content.innerHTML = `
      <slot></slot>
    `

    this.shadowRoot.append(styles)
    this.shadowRoot.append(content)
  }
}

customElements.define('custom-alert', Alert)
