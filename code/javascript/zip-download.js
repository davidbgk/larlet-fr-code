async function GenerateZipDownload() {
  const imageDownload =
    'https://unsplash.com/photos/two-people-in-scuba-gear-swimming-in-the-ocean-SuGTwrtPCg4'
  const file = await fetch(imageDownload).then((r) => r.blob()) // (1)!

  const zip = new JSZip() // (2)!
  zip.file('filename.jpg', file) // adds the image file to the zip file
  const zipData = await zip.generateAsync({ type: 'blob', streamFiles: true })

  const link = document.createElement('a') // (3)!
  link.href = window.URL.createObjectURL(zipData)
  link.download = 'scuba-gear-swimming-data.zip'
  link.click()
}
