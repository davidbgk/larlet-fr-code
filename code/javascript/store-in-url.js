const stateString = JSON.stringify(appState) // appState is a json object
const compressed = compress(stateString)
const encoded = Base64.encode(compressed)
// Push that `encoded` string to the url
// ... Later, on page load or on undo/redo we read the url and
// do the following
const decoded = Base64.decode(encoded) // same encoded as above, but read from url
const uncompressed = uncompress(decoded)
const newState = JSON.parse(uncompressed)
// Now load your application with the newState
