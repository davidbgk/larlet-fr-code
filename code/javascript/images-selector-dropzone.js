;(function openFileSelectorOnDropzoneClick(targetSelector, inputSelector) {
  'use strict'

  const target = document.querySelector(targetSelector)
  const input = document.querySelector(inputSelector)
  target.addEventListener('click', (event) => input.click(), false)
})('canvas', 'input[type="file"]')
