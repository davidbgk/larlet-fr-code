;(function () {
  function stopClickEvent(ev) {
    ev.preventDefault()
    ev.stopPropagation()
  }
  document.body.addEventListener('click', function (ev) {
    if (
      ev.target.tagName === 'A' ||
      ev.target.getAttribute('type').toLowerCase() === 'submit'
    ) {
      setTimeout(function () {
        // Needs to happen _after_ the request goes through, hence the timeout
        ev.target.addEventListener('click', stopClickEvent)
      }, 0)
      setTimeout(function () {
        ev.target.removeEventListener('click', stopClickEvent)
      }, 500) // (1)
    }
  })
})()
