function camelToKebab(s) {
  return s.replace(/[A-Z]/g, (s) => '-' + s.toLowerCase())
}
