function attr(el, name, value = undefined) {
  const curValue = el.getAttribute(name)
  if (typeof name === 'object') {
    for (const at in name) {
      el.setAttribute(camelToKebab(at), name[at]) // (1)!
    }
    return null
  } else if (value === undefined) return el.getAttribute(name)
  else if (value === null) return el.removeAttribute(name), curValue
  else return el.setAttribute(name, value), value
}
