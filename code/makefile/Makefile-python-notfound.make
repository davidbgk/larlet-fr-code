VENV := venv  # (1)!
PY3 := $(shell command -v python3 2> /dev/null)
PYTHON := $(VENV)/bin/python

$(PYTHON):
	@if [ -z $(PY3) ]; then echo "Python 3 could not be found."; exit 2; fi
	$(PY3) -m venv $(VENV)
