from unicodedata import normalize


def compare_strings(s1: str, s2: str) -> bool:
    return normalize("NFKC", s1).casefold() == normalize("NFKC", s2).casefold()
