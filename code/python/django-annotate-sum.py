from django.db import models
from django.db.models.expressions import RawSQL


class MyModelQuerySet(models.QuerySet):
    def annotate_sum(self, field_name, annotation_name):
        raw_query = f"""
            SELECT SUM({field_name})
            FROM {self.model._meta.db_table} AS model
            WHERE -- add condition here
        """
        annotation = {annotation_name: RawSQL(raw_query, [])}
        return self.annotate(**annotation)
