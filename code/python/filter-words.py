def filter_words(text):
    # French stopwords from https://github.com/stopwords-iso/stopwords-fr
    # Walrus operator (:=) requires Python 3.8+.
    return set(
        word_strip_lower
        for word in text.split(" ")
        if (word_strip := word.strip())  # (1)
        and (word_strip_lower := word_strip.lower()) not in STOP_WORDS
        and not word_strip.isdigit()
    )
