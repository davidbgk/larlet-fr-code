import string


def strip_punctuation(text):
    # See https://stackoverflow.com/a/266162 (adapted for French texts).
    # The two last characters are NBSP and NNBSP.
    additional_punctuation = "«»’“”…•·—–  "  # (1)
    # string.punctuation == !"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~
    punctuation = string.punctuation + additional_punctuation
    return text.translate(str.maketrans(punctuation, " " * len(punctuation)))  # (2)
