from pathlib import Path
from urllib.parse import urlencode, urlparse, urlunsplit

import httpx


def convert_sheet_url_to_csv_download_url(urlstring):
    """
    From:
    https://docs.google.com/spreadsheets/d/ID/edit
    To:
    https://docs.google.com/spreadsheets/d/ID/export?format=csv&id=ID
    """
    parse_result = urlparse(urlstring)
    # /spreadsheets/d/ID/edit
    # => ID
    gsheet_id = parse_result.path.split("/")[3]
    url_path = parse_result.path
    url_path = url_path.replace("/edit", "/export")
    params = {"format": "csv", "id": gsheet_id}
    url = urlunsplit(
        (parse_result.scheme, parse_result.netloc, url_path, urlencode(params), "")
    )
    return url


def download_googlesheet_as_csv():
    sheet_url = "https://docs.google.com/spreadsheets/d/ID/edit"  # (1)
    csv_url = convert_sheet_url_to_csv_download_url(sheet_url)
    response = httpx.get(csv_url, follow_redirects=True)
    response.raise_for_status()
    (Path() / "data.csv").write_text(response.text)
