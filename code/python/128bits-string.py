import base64
import uuid


def generate_128bits():
    """Generate a 128bits string, useful for URLs.

    >>> generate_128bits()
    'LQ0HB7ksTdesCuSms-I98Q'
    """
    return base64.urlsafe_b64encode(uuid.uuid4().bytes)[:22].decode()
