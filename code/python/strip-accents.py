import unicodedata


def strip_accents(text):
    # See https://stackoverflow.com/a/518232
    return "".join(
        c
        for c in unicodedata.normalize("NFD", text)
        if unicodedata.category(c) != "Mn"  # fmt: off (1)
    )
