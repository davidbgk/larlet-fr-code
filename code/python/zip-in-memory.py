import io
import json
import zipfile

from django.http import HttpResponse


def render_to_response(self, context, *args, **kwargs):
    zip_buffer = io.BytesIO()
    with zipfile.ZipFile(zip_buffer, "a", zipfile.ZIP_DEFLATED, False) as zip_file:
        for map_ in self.get_maps():
            map_geojson = map_.generate_geojson(self.request)
            geojson_file = io.StringIO(json.dumps(map_geojson))
            file_name = f"umap_backup_{map_.slug}.umap"
            zip_file.writestr(file_name, geojson_file.getvalue())

    response = HttpResponse(zip_buffer.getvalue(), content_type="application/zip")
    response["Content-Disposition"] = 'attachment; filename="umap_backup_complete.zip"'
    return response
