import hashlib


def generate_md5(content):
    """Generate a md5 string from a given string.

    >>> generate_md5("foo")
    'acbd18db4cc2f85cedef654fccc4a4d8'
    """
    return hashlib.md5(content.encode()).hexdigest()
