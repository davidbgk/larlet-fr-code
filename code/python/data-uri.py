import base64
import mimetypes


def img_to_data(path):
    """Convert a file (specified by a path) into a data URI."""
    mime, _ = mimetypes.guess_type(path)
    with open(path, "rb") as fp:
        data = fp.read()
        data64 = "".join(base64.encodestring(data).splitlines())
        return f"data:{mime};base64,{data64}"
