def neighborhood(iterable, first=None, last=None):  # (1)!
    """
    Yield the (previous, current, next) items given an iterable.
    You can specify a `first` and/or `last` item for bounds.
    """
    iterator = iter(iterable)
    previous = first
    current = next(iterator)  # Throws StopIteration if empty.
    for next_ in iterator:
        yield (previous, current, next_)
        previous = current
        current = next_
    yield (previous, current, last)
