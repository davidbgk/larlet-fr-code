import time
from datetime import date


def iso8601toEpoch(iso8601):
    """The name says it all, there has to be a better way.

    >>> iso8601toEpoch('2021-07-11')
    1625976000.0
    """
    return time.mktime(
        date(*[int(part) for part in iso8601.split("-")]).timetuple()
    )  # fmt: off (1)
