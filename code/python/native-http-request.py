import http.client

headers = {
    "content-type": "application/json",
    "authorization": "Bearer json-token",
}
body = """{
    "query": "query($user:ID!){foo}",
    "variables": {"user": "bar"}
}"""

conn = http.client.HTTPSConnection("example.com")
conn.request("POST", "/graphql", body, headers)
res = conn.getresponse()

data = res.read()
print(res.status, res.reason)
print(data.decode("utf-8"))
print(res.getheaders())
