#!/usr/bin/env bash

set -o errexit # (1)!
set -o nounset # (2)!
set -o pipefail # (3)!
if [[ "${TRACE-0}" == "1" ]]; then
    set -o xtrace # (4)!
fi

# (5)!
if [[ "${1-}" =~ ^-*h(elp)?$ ]]; then
    echo 'Usage: ./script.sh arg-one arg-two

This is an awesome bash script to make your life better.

'
    exit
fi

cd "$(dirname "$0")" # (6)!

main() {
    echo do awesome stuff # (7)!
}

main "$@"
