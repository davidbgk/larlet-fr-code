function cd() {
  if [[ -d ./venv ]] ; then
    deactivate
  fi

  builtin cd $1

  if [[ -d ./venv ]] ; then
    . ./venv/bin/activate
  fi

  # (1)!
  if [[ -d ./node_modules ]] ; then
    export PATH="$(npm bin):$PATH"
  fi
}
