rm -rf docs
git init tmp-umap
cd tmp-umap
git remote add origin https://github.com/umap-project/umap
git fetch --depth=1
git config core.sparseCheckout true
echo docs-users >> .git/info/sparse-checkout
git checkout master
cd ..
mv tmp-umap/docs-users docs
rm -rf tmp-umap
